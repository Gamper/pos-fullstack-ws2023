<?php

/**
 * enthält alle Validierungsfehler (assoziatives Array)
 */
$errors = [];

function validateName($name)
{
    global $errors; // Zugriff auf die globale Fehlervariable

    if (strlen($name) == 0) {
        $errors['name'] = "Name darf nicht leer sein";
        return false;
    } else if (strlen($name) > 25) {
        $errors['name'] = "Name zu lang";
        return false;
    } else {
        return true;
    }
}

function validateDate($date)
{
    global $errors; // Zugriff auf die globale Fehlervariable

    // ungültig wenn: leeres Datum, falsches Format oder in der Zukunft
    try {
        if ($date == "") {
            $errors['date'] = "Datum darf nicht leer sein";
            return false;
        } else if (new DateTime($date) > new DateTime()) {
            $errors['date'] = "Datum darf nicht in der Zukunft liegen";
            return false;
        } else {
            return true;
        }
    } catch (Exception $e) {
        $errors['date'] = "Datum ungültig";
        return false;
    }
}

function validateWeight($weight)
{
    global $errors; // Zugriff auf die globale Fehlervariable

    if (!is_numeric($weight) || $weight < 1 || $weight > 500) {
        $errors['weight'] = "Gewicht ungültig";
        return false;
    } else {
        return true;
    }
}

function validateHeight($height)
{
    global $errors; // Zugriff auf die globale Fehlervariable

    if (!is_numeric($height) || $height < 1 || $height > 500) {
        $errors['height'] = "Größe ungültig";
        return false;
    } else {
        return true;
    }
}

function validate($name, $height, $weight, $date)
{
    return validateName($name) & validateHeight($height) & validateWeight($weight) & validateDate($date);
}

/**
 * calculate bmi
 * @param $height in cm
 * @param $weight in kg
 * @return float
 */
function calculateBMI($height, $weight) {
    $height /= 100; // cm -> m
    return $weight / ($height * $height);
}

function getBMIText($bmi) {
    if ($bmi < 18.5) {
        return "Untergewicht";
    } else if ($bmi < 25) {
        return "Normal";
    } else if ($bmi < 30) {
        return "Übergewicht";
    } else {
        return "Adipositas";
    }
}

function getBMIBootstrapClass($bmi) {
    if ($bmi < 18.5) {
        return "alert alert-warning";
    } else if ($bmi < 25) {
        return "alert alert-success";
    } else if ($bmi < 30) {
        return "alert alert-warning";
    } else {
        return "alert alert-danger";
    }
}
