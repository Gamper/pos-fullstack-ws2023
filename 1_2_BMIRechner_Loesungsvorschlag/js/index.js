/**
 * clientseitige Validierung des Datums
 * @param elem input Element
 */
function validateDate(elem) {
    let today = new Date();
    today.setHours(0,0,0,0);      // heutiges Datum ohne Uhrzeit

    let date = new Date(elem.value);          // YYYY-MM-DD format
    date.setHours(0,0,0,0); // Zeitzone bugfix

    if (date <= today) {
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
    } else {
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
    }
}
