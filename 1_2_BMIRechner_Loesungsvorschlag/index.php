<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Body-Mass-Index-Rechner</title>

    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/ampel.js"></script>
</head>
<body onload="initialize()">

<div class="container">

    <h1 class="mt-5 mb-3">Body-Mass-Index-Rechner</h1>

    <?php

    // einbinden/laden der Validierungsfunktionen
    require_once("lib/func.inc.php");
    require_once("lib/db.func.inc.php");

    // Formularvariablen definieren (in PHP nicht zwingend notwendig)
    $name = "";
    $height = "";
    $weight = "";
    $date = "";

    if (isset($_POST['submit'])) {

        // double-check: zuerst pruefen ob die Daten im Request enthalten sein, dann auslesen
        $name = isset($_POST['name']) ? $_POST['name'] : "";
        $height = isset($_POST['height']) ? $_POST['height'] : "";
        $weight = isset($_POST['weight']) ? $_POST['weight'] : "";
        $date = isset($_POST['date']) ? $_POST['date'] : "";

        // Validierung der Daten und Ausgabe des Ergebnisses (an der aktuellen Stelle in der HTML-Seite)
        if (validate($name, $height, $weight, $date)) {
            $bmi = calculateBMI($height, $weight);
            save($name, $date, $bmi);

            echo "<div id='bmi' class='" . getBMIBootstrapClass($bmi) . "'>BMI: " . number_format($bmi, 1) . " - " . getBMIText($bmi) . "</div>";
        } else {
            echo "<div id='bmi' class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
            foreach ($errors as $key => $value) {
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }

    } else {
        echo "<div id='bmi'>&nbsp;</div>";
    }

    ?>

    <form id="form_bmi" action="index.php" method="post">

        <div class="row">

            <div class="col-md-8">

                <div class="row">

                    <div class="col-sm-8 form-group">
                        <label for="name">Name*</label>
                        <input type="text"
                               name="name"
                               class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($name) ?>"
                               maxlength="20"
                               required="required"
                        />
                    </div>

                    <div class="col-sm-4 form-group">
                        <label for="date">Messdatum*</label>
                        <input type="date"
                               name="date"
                               class="form-control <?= isset($errors['date']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($date) ?>"
                               onchange="validateDate(this)"
                               required="required"
                        />
                    </div>

                    <div class="col-sm-6 form-group">
                        <label for="height">Größe (cm)*</label>
                        <input type="number"
                               name="height"
                               class="form-control <?= isset($errors['height']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($height) ?>"
                               min="1"
                               max="300"
                               required="required"
                        />
                    </div>

                    <div class="col-sm-6 form-group">

                        <label for="weight">Gewicht (kg)*</label>
                        <input type="number"
                               name="weight"
                               class="form-control <?= isset($errors['weight']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($weight) ?>"
                               min="1"
                               max="500"
                               required="required"
                        />
                    </div>

                </div>

            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="col-12 border">
                        <h4>Info zum BMI:</h4>
                        Unter 18.5 Untergewicht<br/>
                        18.5 - 24.9 Normal<br/>
                        25.0 - 29.9 Übergewicht<br/>
                        30.0 und darüber Adipositas
                    </div>
                    <div id="ampel" class="col-12">&nbsp;</div>
                </div>
            </div>


        </div>

        <div class="row mt-3">

            <div class="col-sm-3 mb-3">
                <input type="submit"
                       name="submit"
                       class="btn btn-primary btn-block"
                       value="Speichern"
                />
            </div>

            <div class="col-sm-3">
                <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
            </div>

        </div>

    </form>

    <?php
    require("index_statistik.php")
    ?>

</body>
</html>
