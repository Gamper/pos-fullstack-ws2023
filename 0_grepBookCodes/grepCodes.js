/**
 * Eine feine JavaScript-Funktion, um aus den weitergeleiteten Mails des Buchverlags Rheinwerk alle Buchcodes zu extrahieren.
 * Die Konsolenausgabe kann einfach mit einem Texteditor in ein CSV umgewandelt und in Excel weiterverarbeitet werden.
 * Muss gegebenenfalls angepasst werden.
 * Voraussetzung: Das Mail muss in TSN-Mail geöffnet werden, damit alle angehängten Nachrichten als iFrames auf der Webseite angezeigt werden.
 */
function grepCodes()
{
    //Hole alle iFrames...
    let framesList = document.getElementsByTagName("iframe");
    let ergebnis = [];
    for (const iterator of framesList)
    {
        //console.log(iterator);
        //Hole das document.Objekt des aktuellen iFrames
        let doc = document.getElementById(iterator.id).contentWindow.document;

        //Hole alle fettgedruckten Elemente, da auch die Codes so formatiert sind...
        let bElements = doc.getElementsByTagName("b");
        for (const el of bElements) {
                //Die Codes sind genau 19 Zeichen lang
                if (el.innerText.length == 19)
                {
                    ergebnis.push(el.innerText);
                    // console.log(el.innerText);
                }
        }
    }    
    console.log(ergebnis);
}