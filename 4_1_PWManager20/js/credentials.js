console.log('JS geladen');

//Stellt sicher, dass der DOM vollständig geladen ist, bevor das JS ausgeführt wird
document.addEventListener('DOMContentLoaded', function () {

    //Hole alle benötigten Elemente aus dem DOM
    let buttonEl = document.getElementById('btnSearch');
    let searchInputEl = document.getElementById('filter');
    let tableBodyEl = document.querySelector('#credentials');
    let fehlerDivEl = document.querySelector('#fehler');


    //Registrieren eines Event Handlers für das Event Button Click
    buttonEl.onclick = function () {
        //Hole den aktuellen Wert des Inputfeldes
        let searchString = searchInputEl.value;
        
        //Funktion holt die Daten vom Server
        //Ist als async markiert, da beim Aufruf nicht auf das Ergebnis gewartet werden soll
        async function getCredentials() {
            try {
                //Hole die Daten vom Server und warte auf das Ergebnis. Das Promise ist durch das await immer resolved
                let response = await fetch('api/credentials/search/' + searchString);
                //Wandelt die Daten in ein JSON Objekt. Das Promise ist durch das await immer resolved. Es kommt also entweder ein JSON zurück, oder es wird eine Exception geworfen
                let data = await response.json();
                
                //Prüft die Daten auf Sinnhaftigkeit
                if (data.length > 0 && data[0].id >=0) {
                    return data;
                }
                throw new Error("Keine Daten gefunden");
            } catch (error) {
                console.error(error);
                //Fehlermeldung für User anzeigen und ins DOM schreiben
                tableBodyEl.innerHTML = "";
                fehlerDivEl.textContent = "Fehler beim Laden der Daten";
            }
        }

        //Registriere Event-Handler für das Promise von getCredentials
        //Sobald die Daten verfügbar sind, wird die Funktion buildTable aufgerufen und die Daten übergeben
        getCredentials().then(data => buildTable(data));

        // So gehts fix nicht, weil die Funktion asynchron ist und das Ergebnis erst später verfügbar ist. In data liegt als Promise
        // let data = getCredentials();
        // buildTable(data);
    }

    /**
     * Funktion baut die DOM-Tabelle auf
     * @param {Array} daten Array mit den Daten
     * @todo Buttons in die Tabelle einfügen
     * @todo Zu beschreibendes DOM-Element als Parameter übergeben, statt globale Variable zu nutzen
     */
    function buildTable(daten)
    {
        let tableContent = "";
        for (const iterator of daten) {
            console.log(iterator);
            tableContent += "<tr>";
            tableContent += "<td>"+iterator.name+"</td>";
            tableContent += "<td>"+iterator.domain+"</td>";
            tableContent += "<td>"+iterator.cms_username+"</td>";
            tableContent += "<td>"+iterator.cms_password+"</td>";
            tableContent += "<td>Todo: Die Buttons anzeigen</td>";
            tableContent += "</tr>";
        }
        tableBodyEl.innerHTML = tableContent;

        //Alternative Schleifenvariante
        // daten.forEach(element => {
        //     console.log(element);
        // });
    }


    /*Fiese Variant mit gechainten Event-Handlers für verschiedene Promises 
    //Fetch() gibt ein Promise zurück, das durch das then() aufgelöst wird
    //then() gibt ein Promise zurück, das durch das nächste then() aufgelöst wird, dadurch ist ein chaining möglich
    */
    // let response = fetch('api/credentials/');
    // console.log(response);

    // //Event Handler für das erste Promise durch den Fetch Aufruf
    // response.then(function (data) {
    //     if (data.ok == true) {
    //         return data.json();
    //     }
    //      //Event Handler für das zweite Promise durch den json() Aufruf
    //     }).then(function (data) {
    //         console.log(data);
    //     //Event Handler für den Fehlerfall   
    //     }).catch(function (error) {
    //         console.error(error);
    //     });










    // searchInputEl.onkeyup = function () {
    //     let searchString = searchInputEl.value;
    //     console.info("Suchwort: " + searchString);
    // }

}); //Ende DOMContentLoaded