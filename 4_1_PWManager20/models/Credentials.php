<?php

require_once 'DatabaseObject.php';

class Credentials implements DatabaseObject, JsonSerializable
{
    private $id;
    private $name;
    private $domain;
    private $cms_username;  // exact same name as in database (ORM)
    private $cms_password;  // exact same name as in database (ORM)

    private $errors = [];

    public function validate()
    {
        return $this->validateHelper('Name', 'name', $this->name, 32) &
            $this->validateHelper('Domäne', 'domain', $this->domain, 128) &
            $this->validateHelper('CMS-Benutzername', 'cms_username', $this->cms_username, 64) &
            $this->validateHelper('CMS-Passwort', 'cms_password', $this->cms_password, 64);
    }

    /**
     * create or update an object
     * @return boolean true on success
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO credentials (name, domain, cms_username, cms_password) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->domain, $this->cms_username, $this->cms_password));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Saves the object to the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE credentials set name = ?, domain = ?, cms_username = ?, cms_password = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->domain, $this->cms_username, $this->cms_password, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM credentials where id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Credentials');  // ORM
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    /**
     * Get an array of objects from database
     * @param null $filter
     * @return array array of objects or empty array
     */
    public static function getAll($filter = null)
    {
        $db = Database::connect();

        if ($filter != null) {
            $sql = "SELECT * FROM credentials WHERE name LIKE ? OR domain LIKE ? ORDER BY name ASC, domain ASC";
        } else {
            $sql = 'SELECT * FROM credentials ORDER BY name ASC, domain ASC';
        }

        $stmt = $db->prepare($sql);
        if ($filter != null) {
            $stmt->execute(array('%' . $filter . '%', '%' . $filter . '%'));
        } else {
            $stmt->execute();
        }

        // fetch all datasets (rows), convert to array of Credentials-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Credentials');

        Database::disconnect();

        return $items;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM credentials WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    /**
     * helper method for validating strings
     * @param $label used in error message
     * @param $key identification for array element
     * @param $value gonna checked
     * @param $maxLength used for value checking
     * @return boolean true if value is valid, else false
     */
    private function validateHelper($label, $key, $value, $maxLength)
    {
        if ($value === null || $value === '') {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            unset($this->errors[$key]);
            return true;
        }
    }

    /**
     * define attributes which are part of the json output
     * @return array|mixed
     */
    public function jsonSerialize(): mixed
    {
        return [
            "id" => intval($this->id),
            "name" => $this->name,
            "domain" => $this->domain,
            "cms_username" => $this->cms_username,
            "cms_password" => $this->cms_password
        ];
    }   


    public function getId(): int
    {
        return $this->id;
    }


    public function setId($id): null
    {
        $this->id = $id;
    }


    /**
     * @return mixed|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed|string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed|string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return mixed|string
     */
    public function getCmsUsername()
    {
        return $this->cms_username;
    }

    /**
     * @param mixed|string $cmsUsername
     */
    public function setCmsUsername($cmsUsername)
    {
        $this->cms_username = $cmsUsername;
    }

    /**
     * @return mixed|string
     */
    public function getCmsPassword()
    {
        return $this->cms_password;
    }

    /**
     * @param mixed|string $cmsPassword
     */
    public function setCmsPassword($cmsPassword)
    {
        $this->cms_password = $cmsPassword;
    }

    /**
     * @return boolean
     */
    public function hasError($field)
    {
        return !empty($this->errors[$field]);
    }

    /**
     * @return array
     */
    public function getError($field)
    {
        return $this->errors[$field];
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}
