# Passwortmanager 2.0 - Musterlösung - php41

Dies ist die Musterlösung zur Aufgabe "Passwortmanager 2.0".
Aufbauend auf den Grundlagen des Datenbankzugriffs und Object-Relational-Mapping (ORM) werden hier das Model-View-Controller (MVC) Pattern und AJAX behandelt.


API Endpoint Doku:
Bitte Pfade an eigenen Server anpassen. Kann mit Postman oder der VSCode Extension Thundeclient getestet werden.


Liefere alle Credentials
HTTP GET
= CRUD Read = Model getAll
http://localhost//POS-Fullstack/pos-fullstack-ws2023/4_1_PWManager20/api/credentials/


Liefere ein Credential mit der ID 1
HTTP GET
= CRUD Read = Model Methode get
http://localhost//POS-Fullstack/pos-fullstack-ws2023/4_1_PWManager20/api/credentials/1


Lege neue Credendials an
HTTP POST
= CRUD Create = Model Methode create
Austauschformat: JSON im HTTP Body. Könnte auch XML oder CSV sein.
http://localhost//POS-Fullstack/pos-fullstack-ws2023/4_1_PWManager20/api/credentials/


Ändere ein Credential mit der ID 1
HTTP PUT 
= CRUD Update = Model Methode update
http://localhost//POS-Fullstack/pos-fullstack-ws2023/4_1_PWManager20/api/credentials/1
JSON im Body


Lösche ein Credential mit der ID 1
HTTP DELETE
= CRUD Delte = Model Methode delete
http://localhost//POS-Fullstack/pos-fullstack-ws2023/4_1_PWManager20/api/credentials/1
