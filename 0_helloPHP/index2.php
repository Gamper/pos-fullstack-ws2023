<?php
    $name = 'PHP';

    //Entweder mit Variablensubstitution...
    $webseitenTitel = "Hallo $name";
    //...oder mit String Concatination
    $webseitenTitel = 'Hello again,' . $name;
    $webseitenInhalt = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam provident, quia pariatur debitis dolores nemo. Distinctio quo, beatae rerum sunt eum animi ab culpa sint, asperiores atque quidem in rem! Consequuntur, dolorem. A non sunt iure temporibus exercitationem quod animi quas natus. Eligendi quidem dignissimos magni.";

    //Indiziertes Array = stinknormales Array
    $namesliste = ['Sinan', 'Kathi', 'Sandro', 'AAron', 'Kathi'];
    
    //Assoziatives Array = Map
    $zweiteListe['vorname'] = 'Seppi';
    $zweiteListe['nachname'] = 'Unterwurzacher';

    var_dump($namesliste);
    var_dump($zweiteListe);

    array_push($namesliste, 'Michi');
    array_push($namesliste, 'Ötzi');
    sort($namesliste);

    printULofNames($namesliste);


    function printULofNames(iterable $liste)
    {
        echo "<ul>";
        foreach ($liste as $key => $value) {
            echo "<li>" . $key  . ": " . $value . "</li>";
        }
        echo "</ul>";
    }
 

    /*
        Ein mehrzeiliges
        Kommentar
    */

    //Konstante definieren
    define('AUTOR', 'Michael Gamper');

    //Definition einer Funktion
    /**
     * Addiert zwei ganzzahlige Zahlen miteinander
     * @param $i Die erste aufzusummierende Zahl
     * @param $j zweite aufzusummierende Zahl
     * @return Liefert die Summe als Ganzzahl
     */
    function addition(int $i, int $j): int
    {
        return $i + $j;
    }


    //Aufruf einer Funktion und Speichern der Returnsin einer Variable
    $ergebnis = addition(3,4);
  
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php 
            echo $webseitenTitel;
        ?>

    </title>
</head>
<body>


    <h1>
        <?php
            echo $webseitenTitel;

        ?>
    </h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam provident, quia pariatur debitis dolores nemo. Distinctio quo, beatae rerum sunt eum animi ab culpa sint, asperiores atque quidem in rem! Consequuntur, dolorem. A non sunt iure temporibus exercitationem quod animi quas natus. Eligendi quidem dignissimos magni.</p>
</body>
</html>

