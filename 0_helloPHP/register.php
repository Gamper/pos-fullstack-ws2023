<?php
    //Ausgabe der rohen POST-Daten
    //var_dump($_POST);

    //1. Schritt: Formulardaten entgegennehmen
    //2. Schritt: Formulardaten validieren
    //3. Schritt: Verarbeiten
    //4. Schritt: Ausgeben in DB oder auf Webseite oder Webservice,...


    /**
     * Prüft, ob ein String ein möglicher Vor- oder Nachname ist
     * 
     * @param string $possiblePersonsName Der zu überprüfende Name
     * @return bool true, wenn die Prüfung erfolgreich war
     * @todo Seltsame Vornamen, z.B. von den Musk-Kindern, werden derzeit nicht detektiert
     */
    function checkName(string $possiblePersonsName): bool
    {
        //Regulärer Ausdruck für Namen von regex101.com
        $pattern = '/^((?:(?:[a-zA-Z]+)(?:-(?:[a-zA-Z]+))+)|(?:[a-zA-Z]+))$/m';
        
        //Prüfe, ob das Suchmuster gefunden wird...
        if (preg_match($pattern, $possiblePersonsName) === 1)
        {
            return true;
        } 
        return false;
    }



    /**
     * Einfache Validierungsfunktion für die PW-Stärke
     * 
     * @param string Das zu überprüfende Passwort
     * @return bool true, wenn das Passwort stark genug ist
     */
    function checkPWStrength(string $pw): bool
    {
        if (strlen($pw) >= 9)
        {
            return true;
        }
        return false;
    }


    /**
     * Validiert Email-Adressen
     * 
     * @param string Eine zu prüfende Mailadresse
     * @return true, wenn die Email-Adresse gültig ist
     */
    function checkEmail(string $tocheck): bool
    {
        $pattern = '/(^[a-zA-Z0-9_.]+[@]{1}[a-z0-9]+[\.][a-z]+$)/m';
        //=== Vergleicht den Wert UND den Typ
        //== Vergleicht nur den Wert
        if (preg_match($pattern, $tocheck) === 1)
        {
            return true;
        } 
        return false;
    }


    //Zugriff auf das assiziative Array $_POST an der Stelle vorname, etc.
    //Die Funktion empty() prüft, ob in die Variable definiert ist und ob etwas drinnensteht
    if (!empty($_POST['vorname']) && !empty($_POST['nachname']) && !empty($_POST['email']) && !empty($_POST['passwort']))
    {
        //Wenn überall Daten vorhanden sind...
        $firstname = !empty($_POST['vorname']) ? $_POST['vorname'] : '';
        $lastname = $_POST['nachname'];
        $email = $_POST['email'];
        $pw = $_POST['passwort'];

        //Selbstgestrickte Lösung mit billigem Salt und zu schnellem Hashalgorithmus, der wahrscheinlich in Zukunft nicht mehr sicher sein wird.
        $salt = $email;
        $popeligerHash = sha1($email . $pw);

        //Gute Lösung mit aktueller, langsamer Hash-Funktion und gutem, zufälligen Salt. 
        //Funktion liefert einen String mit Algorithmus, Salt und dem eigentlichen Hash
        $guterHash = password_hash($pw, PASSWORD_DEFAULT);
        password_verify($pw, $guterHash);

        echo $popeligerHash;
        echo '<br />';
        echo $guterHash;

        //...validiere die Daten: prüfen, ob wirklich etwas Sinnvolles drinnensteht.
        //In diesem Fall rufe ich meine eigenen Validierungsfunktionen auf
        //Alternativ könnte man auch vordefinierte aufrufen, z.B: filter_input(...)
        
        //Ein leeres Array, in dem alle Fehler zusammengesammelt werden.
        $errors = array();

        if (checkEmail($email) === true)
        {
            //Debug-Ausgabe:
            echo "DEBUG: Email-Adresse ist gültig.";
        }
        else{
            //Debug-Ausgabe:
            echo "DEBUG: Email-Adresse ist nicht gültig.";

            //Füge Fehlermeldung dem Array hinzu.
            array_push($errors, "Email-Adresse ist NICHT gültig.");
            //Todo: Umleiten auf login.php und Anzeigen der Fehlermeldung
        }

        if (checkPWStrength($pw) === true)
        {
             //Debug-Ausgabe:
            echo "DEBUG: Passwort stark genug.";
        }
        else{
             //Debug-Ausgabe:
            echo "DEBUG: Passwort ist zu schwach!";
            array_push($errors, "Passwort ist zu schwach!");
        }

        if (checkName($firstname) === true)
        {
             //Debug-Ausgabe:
            echo "DEBUG: Vorname gültig.";
        }
        else{
             //Debug-Ausgabe:
            echo "DEBUG: Vorname nicht gültig.";
            array_push($errors, "Vorname nicht gültig.");
        }

        if (checkName($lastname) === true)
        {
             //Debug-Ausgabe:
            echo "DEBUG: Nachname gültig.";
        }
        else{
             //Debug-Ausgabe:
            echo "DEBUG: Nachname nicht gültig.";
            array_push($errors, "Nachname nicht gültig.");
        }

        //TODO: Validierung von Vorname und Nachname
                // checkName($firstname);
                // checkName($lastname);

        //Wenn Fehler aufgetreten sind
        if (sizeof($errors) == 0)
        {
            //Filtern der Ausgabe. Website defacement und session hijacking verhindert
            echo '<h1>Vielen Dank für die Registrierung, ' . htmlspecialchars($firstname) . '</h1>';
            echo '<a href="index.php    ">Weiter zur Startseite</a>';
            //Todo: Umleiten auf login.php
        }        
        else {
            //Durchlaufe das Array der Fehlermeldungen und gib diese aus
            //Todo: Fehlermeldungen im Formular ausgeben
            echo '<ul>';
            foreach ($errors as $key => $value) {
                echo '<li>' . $errors[$key] . '</li>';
            }
            echo '</ul>';
            echo '<a href="register_form.html">Zurück zum Registrierungsformular</a>';
        } 
       
    }
    else
    {
        //Todo: Umleiten auf login.php
    }
   
?>