<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Anmeldeformular</title>
</head>

<body>
  <?php
    // Zusätzliche Validierung der Formulardaten
    define(
        'PASSWORD_SALTI',
        'wJMM4|{UT<&r<*~%.b:AomWvw21(B5Gc_m:uSk^f,4bWHHMw|Su>@?5F7D4g)>~H'
      );
  if (!empty($_POST)) {
    // 2. Schritt: Validierung
    $formFields = [
      'firstname' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
      'lastname' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
      'email' => FILTER_SANITIZE_EMAIL | FILTER_VALIDATE_EMAIL,
      'password' => [
        'filter' => FILTER_CALLBACK,
        'flags' => FILTER_REQUIRE_SCALAR,
        'options' => 'hashPassword'
      ],
      'browser' => [
        'filter' => FILTER_CALLBACK,
        'flags' => FILTER_REQUIRE_SCALAR,
        'options' => 'enumBrowsers'
      ],
      'feedback' => FILTER_VALIDATE_BOOLEAN,
      'improvements' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
      'newsletter' => FILTER_VALIDATE_BOOLEAN
    ];
    $formData = filter_input_array(
      INPUT_POST,
      $formFields
    );



    $requiredFields = [
      'firstname',
      'lastname',
      'email',
      'password'
    ];

    $errorFields = [];

    foreach ($requiredFields as $requiredField) {
      if (empty($formData[$requiredField])) {
        $errorFields[] = $requiredField;
      }
    }

    if (count($errorFields) > 0) {
      echo '<p><strong>Bitte füllen Sie die folgenden Felder aus: '
        . implode(', ', $errorFields) . '!</strong></p>';
    } else {
      $sendData = $formData;
      array_walk($sendData, fn (&$val, $key) => $val = $key . ': ' . $val);
      $message = implode("\r\n", $sendData);

      $mailSent = mail(
        'chrstecher@tsn.at',
        'Mail vom Anmeldeformular',
        wordwrap($message, 70, "\r\n")
      );

      if (!$mailSent) {
        echo '<p><strong>Leider konnte die E-Mail' . 'nicht versandt werden. </strong></p>';
      } else {
        echo '<p><strong>E-Mail wurde versandt!</strong></p>';
      }
    }
  }
  ?>

  <?php



  function enumBrowsers($value): ?string
  {
    $possibleValues = [
      'chrome',
      'edge',
      'firefox',
      'opera',
      'safari',
    ];

    foreach ($possibleValues as $possibleValue) {
      if ($possibleValue === $value) {
        return $value;
      }
    }

    return null;
  }

  function hashPassword($password): string
  {
    return !empty($password) ? crypt($password, PASSWORD_SALTI) : '';
  }

  hashPassword("testitest");
  ?>

  <form action="" method="POST">
    <fieldset>
      <legend>Angaben zur Person</legend>
      <label>
        Vorname:
        <input type="text" name="firstname" size="20" maxlength="50" />

      </label> <br>
      <label>
        Nachname:
        <input type="text" name="lastname" size="30" maxlength="70" />
      </label> <br>
      <label>
        E-Mail:
        <input type="email" name="email" size=30 maxlength=70 />
      </label> <br>
      <label>
        Passwort:
        <input type="password" name="password" size=20 maxlength="64" />
      </label>
    </fieldset> <br>
    <fieldset>
      <legend>Fragebogen</legend>
      <br>
      <label for="browser">Welchen Browser nutzen Sie?</label>
      <select name="browser">
        <option value="chrome">Google Chrome</option>
        <option value="firefox">Firefox</option>
        <option value="edge">Microsoft Edge</option>
        <option value="opera">Opera</option>
        <option value="safari">Safari</option>
      </select>
      <br>
      <br>
      <legend>Gefällt Ihnen unsere Website?</legend>
      <input type="radio" name="feedback" value="ja">
      <label for="ja">Ja</label>
      <input type="radio" name="feedback" value="nein">
      <label for="nein">Nein</label><br><br><br>
      <legend>Haben Sie Verbesserungsvorschläge?</legend>
      <textarea name="improvements" rows="5" cols="60"></textarea><br><br>
      <input type="checkbox" name="newsletter" value="newsletter">
      <label for="newsletter">Möchten Sie sich für unseren Newsletter anmelden?</label><br><br>
    </fieldset>
    <br>
    <input type="submit" value="Formular abschicken" />
  </form>
</body>

</html>

