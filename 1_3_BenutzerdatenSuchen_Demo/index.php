<?php

//Nimmt die Formulardaten entgegen
if (isset($_GET["search"])) {
    $suchwort = $_GET["search"];
    //Todo validieren
    //Todo: Durchsuche DB-Daten nach Suchwort (rufe getFilteredData auf)
}


/**
 * Hole alle Daten "roh" aus der Datenquelle
 */
function getAllData()
{
    //TODO: Lesender DB-Zugriff mit SELECT
    require('data/data.php');
    return $data;
}



function getFilteredData($suchwort)
{
    $data = getAllData();
    foreach ($data as $key => $value) {
        //Todo: Zeilenweise mit Suchwort vergleichen
    }
}


/**Erzeuge aus den Rohdaten eine HTML-Tabelle
 * 
 */
function getAllUserAsHTMLTable()
{
    $data = getAllData();
    $html = '<table>';
    $html .=  '<th>User</th>';
    $html .=  '<th>Email</th>';
    $html .=  '<th>Geburtsdatum</th>';
    foreach ($data as $key => $value) {
        // var_dump($value['firstname']);
        $html .=  '<tr>';
        $html .=  '<td><a href="benutzerdetails.php?id='. $value['id'] .'">'.$value['firstname']. ' ' .$value['lastname'] . '</a></td>';
        $html .=  '<td>'.$value['email'].'</td>';
        $html .=  '<td>'.$value['birthdate'].'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    return $html;
}


?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Benutzerdatensuche</title>
</head>
<body>
    <form method="get" action="index.php">
        <label for="suche">Suche</label>
        <input type="text" id="suche" name="search" />
        <input type="submit" value="Suchen" />
    </form>

    <?php
        echo getAllUserAsHTMLTable();

    ?>
</body>
</html>


