<?php

require_once('controllers/Controller.php');

$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['home'];
$controller = sizeof($route) > 0 ? $route[0] : 'home';

if ($controller == 'home')
{
    require_once('controllers/HomeController.php');
    (new HomeController())->handleRequest($route);
}
else
{
    Controller::showError("Page not found", "Page for operation " . $controller . " was not found!");
}

?>
