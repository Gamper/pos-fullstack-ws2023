<?php

require_once('RESTController.php');

class CurrentStatsRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            // case 'POST':
            //     $this->handlePOSTRequest();
            //     break;
            // case 'PUT':
            //     $this->handlePUTRequest();
            //     break;
            // case 'DELETE':
            //     $this->handleDELETERequest();
            //     break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }

    /**
     * get single/all stations
     * single station: GET api.php?r=/station/25 -> args[0] = 25
     * all stations: GET api.php?r=station
     * all measurements of single station: GET api.php?r=/station/2/measurement -> arg[0] = 2, args[1] = measurement
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = file_get_contents('models/CurrentStats.json');            
            echo $model;
            
            //$this->response($model);
        } else if ($this->verb == null && sizeof($this->args) == 1) {
          
        } else if ($this->verb == null && sizeof($this->args) == 2 && $this->args[1] == 'measurement') {
          
        } else {
            $this->response("Bad request", 400);
        }
    }

  

}
