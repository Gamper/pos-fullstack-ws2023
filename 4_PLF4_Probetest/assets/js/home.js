document.addEventListener('DOMContentLoaded', function(){
  
    let datePickerEl = document.getElementById('datepicker');
    // console.log(datePickerEl);

    //Event handler für das Event onchange registrieren
    //Wird ausgeführt, sobald in diesem Element der Wert geändert wird.
    datePickerEl.onchange = dateHandler;
    
    function dateHandler(){
      let chosenDate = datePickerEl.value;
      console.log(chosenDate);
      //Filter der Daten
      getSalesPerDay();
    };



    async function getCurrentStats()
    {
        let response = await fetch('api/currentstats');
        let json = await response.json();
        let wert = json.today.anzahlverkaeufe;
        //Tu was mit der Website: DOM Manipulation
        updateCards(wert);
    }

    async function getSalesPerDay()
    {
        let response = await fetch('api/salesperday');
        let json = await response.json();
        // console.log(json);
        updateTable(json);
    }
    getSalesPerDay();

    function updateTable(data)
    {
      //Hole die Tabelle
      let tableBodyEl = document.getElementById('tablebody');
      // console.log(tableBodyEl);

      let tableTemplate = '';
      let xAchse = [];
      let chartData = [];
      for (const tag of data) {
        // console.log(tag);
        let datum = tag.Datum;
        xAchse.push(tag.Datum);
        chartData.push(tag.Eintraege.length);
      
        for (const gericht of tag.Eintraege) {
          //Erzeuge Tabellenzeilen
          tableTemplate = tableTemplate + '<tr>';
          tableTemplate += '<td>'+ datum +'</td>';
          tableTemplate += '<td>'+ gericht.Gericht + '</td>';
          tableTemplate += '<td>'+ gericht.Preis + '</td>';
          tableTemplate += '<td>'+ gericht.Kategorie +'</td>';
          tableTemplate = tableTemplate + '</tr>';
          // console.log(gericht);
        }
      }
      console.log(xAchse);
      console.log(chartData);
      fillLineChart(xAchse, chartData);
      //Füge die Tabellenzeilen der Tabelle hinzu
      tableBodyEl.innerHTML = tableTemplate;
    }




    getCurrentStats();

    function updateCards(data)
    {
      //Hole die Cards
      // document.querySelector('#anzahl'); oder:
      let h6El = document.getElementById('anzahl');
      //Verändere die Cards
      h6El.textContent = data;
    }

    // fillLineChart();

    /**
     * Füllt das Liniendiagramm mit aktuellen Werten.
     */
    function fillLineChart(achsbeschriftung, daten)
    {
        //console.log(data);
        new ApexCharts(document.querySelector("#reportsChart"), {
            series: [{
              name: 'Anzahl Verkäufe',
              data:  daten,
            }, {
              name: 'Umsatz',
              data: [11, 32, 45, 32, 34, 52, 41]
            }],
            chart: {
              height: 350,
              type: 'area',
              toolbar: {
                show: false
              },
            },
            markers: {
              size: 4
            },
            colors: ['#4154f1', '#2eca6a', '#ff771d'],
            fill: {
              type: "gradient",
              gradient: {
                shadeIntensity: 1,
                opacityFrom: 0.3,
                opacityTo: 0.4,
                stops: [0, 90, 100]
              }
            },
            dataLabels: {
              enabled: false
            },
            stroke: {
              curve: 'smooth',
              width: 2
            },
            xaxis: {
              type: 'string',
              categories: achsbeschriftung
            },
            tooltip: {
              x: {
                //format: 'dd.MM.yy'
              },
            }
          }).render();
    }




 


})