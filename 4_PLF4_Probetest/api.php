<?php

require_once('controllers/RESTController.php');


$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['measurement'];
$controller = sizeof($route) > 0 ? $route[0] : 'measurement';

if ($controller == 'currentstats') {
    require_once('controllers/CurrentStatsRESTController.php');

    try {
        (new CurrentStatsRESTController())->handleRequest();
    } catch (Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
} else if ($controller == 'salesperday') {
    require_once('controllers/SalesRESTController.php');

    try {
        (new SalesRESTController())->handleRequest();
    } catch (Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
} else {
    RESTController::responseHelper('REST-Controller "' . $controller . '" not found', '404');
}
