[
  {
    "Datum": "2023-06-01",
    "Uhrzeit": "12:30:00",
    "Gericht": "Rinderfilet",
    "Kategorie": "Hauptspeise",
    "Preis": 25.99
  },
  {
    "Datum": "2023-06-02",
    "Uhrzeit": "13:15:00",
    "Gericht": "Caprese-Salat",
    "Kategorie": "Vorspeise",
    "Preis": 8.50
  },
  {
    "Datum": "2023-06-03",
    "Uhrzeit": "14:00:00",
    "Gericht": "Tiramisu",
    "Kategorie": "Nachspeise",
    "Preis": 7.99
  },
  {
    "Datum": "2023-06-04",
    "Uhrzeit": "11:45:00",
    "Gericht": "Espresso",
    "Kategorie": "Kaffee",
    "Preis": 2.50
  },
  {
    "Datum": "2023-06-05",
    "Uhrzeit": "12:15:00",
    "Gericht": "Lachsfilet",
    "Kategorie": "Hauptspeise",
    "Preis": 19.99
  },
  {
    "Datum": "2023-06-06",
    "Uhrzeit": "11:30:00",
    "Gericht": "Caesar-Salad",
    "Kategorie": "Vorspeise",
    "Preis": 7.50
  },
  {
    "Datum": "2023-06-07",
    "Uhrzeit": "14:30:00",
    "Gericht": "Schokoladenkuchen",
    "Kategorie": "Nachspeise",
    "Preis": 6.99
  },
  {
    "Datum": "2023-06-08",
    "Uhrzeit": "13:00:00",
    "Gericht": "Cappuccino",
    "Kategorie": "Kaffee",
    "Preis": 3.00
  },
  {
    "Datum": "2023-06-09",
    "Uhrzeit": "12:45:00",
    "Gericht": "Hühnchenbrust",
    "Kategorie": "Hauptspeise",
    "Preis": 16.99
  },
  {
    "Datum": "2023-06-10",
    "Uhrzeit": "11:30:00",
    "Gericht": "Gemischter Salat",
    "Kategorie": "Vorspeise",
    "Preis": 6.50
  },
  {
    "Datum": "2023-06-11",
    "Uhrzeit": "14:15:00",
    "Gericht": "Erdbeertorte",
    "Kategorie": "Nachspeise",
    "Preis": 8.99
  },
  {
    "Datum": "2023-06-12",
    "Uhrzeit": "12:30:00",
    "Gericht": "Latte Macchiato",
    "Kategorie": "Kaffee",
	"Preis": 3.50
  }