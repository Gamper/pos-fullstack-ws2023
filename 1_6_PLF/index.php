<?php
    $azeit = isset($_POST["ausleihzeit"]) ? $_POST["ausleihzeit"] :"";
    $ezeit = isset($_POST["endzeit"]) ? $_POST["endzeit"] :"";
    
   
    $az = new DateTime($azeit);
    $ez = new DateTime($ezeit);



    var_dump(checkEZeit($az, $ez));


    function checkAzeit($az)
    {
        $zeit = new DateTime();
        $interval = new DateInterval('PT2H');
        if ($zeit->add($interval) >= $az)
        {
            return true;
        }
        return false;
    }

    function checkEZeit($az, $ez)
    {
        $az->format("%D");
    }

?>


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.css" rel="stylesheet">
    <title>Scooter</title>
</head>
<body>
<header class="bg-image" style="background-image: url('img/scooter.jpg'); height: 20vh; background-size: contain;">
    <h1 class="text-center fw-bold">Scooter4You</h1>
</header>
<form class="container" action="index.php" method="post">
  <div class="mb-3">
    <label for="email" class="form-label">Email-Adresse</label>
    <input type="email" class="form-control" id="email" placeholder="maxine.masters@test.at">
  </div>
  <div class="mb-3">
    <label for="start" class="form-label">Ausleihzeitpunkt</label>
    <input type="datetime-local" name="ausleihzeit" class="form-control" id="start">
  </div>
  <div class="mb-3">
    <label for="end" class="form-label">Rückgabezeitpunkt</label>
    <input type="datetime-local" name="endzeit" class="form-control" id="end">
  </div>
  <div class="mb-3">
    <label for="scooter" class="form-label">E-Scooter auswählen</label>
    <select class="form-select" id="scooter">
      <option selected>Bitte wählen Sie einen E-Scooter aus</option>
      <option value="1">E-Scooter "D'Resl": Andreas-Hofer-Straße</option>
      <option value="2">E-Scooter "Max": Maximilianstraße</option>
      <option value="3">E-Scooter "Der Hofer wars": Südtirolerplatz </option>
      <option value="4">E-Scooter "Hochgoggeler": Botznerplatz</option>
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Scooter reservieren</button>
</form>


</body>
</html>