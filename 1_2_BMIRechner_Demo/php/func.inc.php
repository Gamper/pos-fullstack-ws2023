<?php

$errors = array();

function validate($name, $gewicht, $groesse, $datum)
{
    //Todo: Validieren
    return (validateName($name) && validateDate($datum) && validateGewicht($gewicht) && validateGroesse($groesse)) ? true : false;
}

function validateName($name)
{
    if (strlen($name) > 3 && strlen($name) < 50)
    {
        return true;
    }
    global $errors;
    array_push($errors,"Name: bitte richtigen Namen eingeben!");
    // $errors[] = 'Name: bitte richtigen Namen eingeben!';
    return false;
}

function validateGewicht($gewicht)
{
    if ($gewicht > 0 && $gewicht <250)
    {
        return true;
    }
    else{
        global $errors;
        // $errors[] = 'Gewicht: bitte realistisches Gewicht eingeben!';
        array_push($errors,'Gewicht: bitte realistisches Gewicht eingeben!');
        return false;
    }
}


function validateGroesse($groesse)
{

    if ($groesse > 0 && $groesse <250) 
    {
        return true;
    }
  
        global $errors;
        array_push($errors,'Größe: Bitte wählen Sie einen realistischen Wert, Sie Joker!');
        // $errors[] = "Größe: Bitte wählen Sie einen realistischen Wert, Sie Joker!";
        return false;
    
}

function validateDate($datum)
{
    $date = new DateTime($datum);
    $currentDate = new DateTime();
    // var_dump($date->format("Y-m-d"));
    // var_dump($currentDate->format("Y-m-d"));
    return ($date->format("Y-m-d") == $currentDate->format("Y-m-d")) ? true : false;
}

function calcBMI($gewicht, $groesse)
{
    $groesseInMeter = $groesse * 0.01;
    $bmi = $gewicht / ($groesseInMeter*$groesseInMeter);
    return $bmi;
}