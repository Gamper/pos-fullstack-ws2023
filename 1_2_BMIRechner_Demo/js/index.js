//Reistriere Event-Handler. Sobald die Seite geladen wurde...
document.addEventListener("DOMContentLoaded", function(){
    // Ausgabe in der Konsole im Browser (F12)
    console.log("Hallo, Webseite wurde komplett geladen");

    //Hole die nötigen Objekte aus dem DOM
    let buttonEl = document.getElementById('jsBtn');
    let heightEl = document.getElementById('grosseCm');
    let weightEl = document.getElementById('gewichtKg');

    /*Variante 1: Registriere Eventhandler/Callback-Funktion
      für das Klick-Event,
      indem die Eigenschaft des Buttons mit einer Funktionsdeklaration
      befüllt wird.*/
    // buttonEl.onclick = function(){
    //     console.log("Button geklickt!");
    // };

    /*Variante 2: Registrierung des Callbacks über die
    addEventListener-Methode für Button und die beiden Eingabefelder für Größe und Gewicht */
    buttonEl.addEventListener('click', bmiAnzeige);
    heightEl.addEventListener('keyup', bmiAnzeige);
    heightEl.addEventListener('keyup', valHeight);
    weightEl.addEventListener('keyup', bmiAnzeige);


    console.log(buttonEl);
    
    function valHeight()
    {
        let divEl = document.getElementById("grDiv");
        
        if (checkHeight(this.value))
        {
            console.log("Checked");
            divEl.style = "display:none";        
        }
        else{
            divEl.style = "display:block";
        }
    }


    /**
     * Funktion holt sich die benötigten Werte aus dem DOM 
     */
    function bmiAnzeige(){
        //console.log("Button wurde geklickt!");

        //Hole das DOM-Objekt mit der ID name
        let nameEl = document.getElementById('name');
        //console.log(nameEl.value);

        //Auslesen der richtigen Eigenschaft.
        let name = nameEl.value;

        //Wenn die Validierung passt...
        if (checkName(name) && checkHeight(heightEl.value) && checkWeight(weightEl.value))
        {
            console.log("Alles super validiert, " +  name);
            let bmi = calcBMI(heightEl.value, weightEl.value);
            
            // document.querySelector("#ergebnis");
            let ergebnisEl = document.getElementById("ergebnis");
            ergebnisEl.innerText = "Ihr BMI-Wert: " + bmi;
            //console.log(ergebnisEl);
        }
        else{
            console.log("Mull isch des!");
            //TODO: Passende Fehlerbehandlung
        }
    };


    /**
     * Berechnet aus zwei Werten den BMI und gibt diesen zurück
     * @param {} groesse 
     * @param {*} gewicht 
     * @returns der berechnete BMI-Wert
     */
    function calcBMI(groesse, gewicht)
    {
        let groesseInMeter = groesse*0.01;
        let bmi = gewicht / (groesseInMeter*groesseInMeter);
        return bmi;
    }


    function checkName(name)
    {
        //Einfache Validierung über die Länge
        if(name.length > 3)
        {
            return true;
        }
        return false;
    }


    function checkHeight(height)
    {
        if (height > 50 && height <300)
        {
            return true;
        }
        return false;
    }

    function checkWeight(weight)
    {
        if (weight > 0 && weight <300)
        {
            return true;
        }
        return false;
    }
    

    //Lesender DOM-Zugriff
    // 
    // 

});




