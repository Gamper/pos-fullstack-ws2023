<?php
//1. Schritt: Nimm die Formulardaten entgegen
$name = isset($_POST['name']) ? $_POST['name'] :'';
$gewicht = isset($_POST['gewicht']) ? $_POST['gewicht'] :'';
$groesse = isset($_POST['groesse']) ? $_POST['groesse'] :'';
$datum = isset($_POST['datum']) ? $_POST['datum'] :'';

//2. Schritt: Validiere die Daten
require_once('php/func.inc.php');
if (validate($name, $gewicht, $groesse, $datum))
{
    //3. Schritt: Berechne den BMI
    $bmi = calcBMI($gewicht, $groesse);
    echo $bmi;  
}
else{
    //echo "Validierungsfehler!";
    if(!empty($errors))
    {
        foreach ($errors as $key => $value) {
            echo $value;
        }
    }
}

//4. Schirtt: Umleiten

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>BMI-Rechner</title>
    <script src="js/index.js"></script>
</head>
<body>
    <header class="h1 text-center">Body-Mass-Index Rechner</header>

    <div class="container px-5 my-5">
        <!--TODO: Form laut Wireframe mit Bootstrap anpassen -->
        <form action="index.php" method="post" id="contactForm">
            <div class="mb-3">
                <label class="form-label" for="name">Name</label>
                <input class="form-control" name="name" id="name" type="text" placeholder="Max Mustermann" data-sb-validations="required" />
                <div class="invalid-feedback" data-sb-feedback="name:required">Name is required.</div>
            </div>
            <div class="mb-3">
                <label class="form-label" for="messdatum">Messdatum</label>
                <input class="form-control" name="datum" id="messdatum" type="date" placeholder="Messdatum" data-sb-validations="required" />
                <div class="invalid-feedback" data-sb-feedback="messdatum:required">Messdatum is required.</div>
            </div>
            <div class="mb-3">
                <label class="form-label" for="grosseCm">Größe (cm)</label>
                <input class="form-control" name="groesse" id="grosseCm" type="number" placeholder="Größe (cm)" data-sb-validations="required" />
                <div id="grDiv" class="invalid-feedback" data-sb-feedback="grosseCm:required">Größe (cm) is required.</div>
            </div>
            <div class="mb-3">
                <label class="form-label" for="gewichtKg">Gewicht (kg)</label>
                <input class="form-control" name="gewicht" id="gewichtKg" type="number" placeholder="Gewicht (kg)" data-sb-validations="required" />
                <div class="invalid-feedback" data-sb-feedback="gewichtKg:required">Gewicht (kg) is required.</div>
            </div>
            <div class="d-none" id="submitSuccessMessage">
                <div class="text-center mb-3">
                    <div class="fw-bolder">Form submission successful!</div>
                    <p>To activate this form, sign up at</p>
                    <a href="https://startbootstrap.com/solution/contact-forms">https://startbootstrap.com/solution/contact-forms</a>
                </div>
            </div>
            <div class="d-none" id="submitErrorMessage">
                <div class="text-center text-danger mb-3">Error sending message!</div>
            </div>
            <div class="d-grid">
                <button class="btn btn-primary btn-lg" id="submitButton" type="submit">Berechne Server-seitig mit PHP</button>

            </div>
        </form>
        <div class="row mt-4">
            <div class="col-sm-4">
                <button class="btn btn-primary btn-lg" id="jsBtn">Berechne Client-seitig mit JS</button>
            </div>
            <div class="col-sm-8">
                <div class="alert alert-info" id="ergebnis">Ergebnis: </div>
            </div>
        </div>

        <div class="row mt-12 alert alert-warning">
        Servervalidierung: <br />
           <?php
                if(isset($errors))
                {
                    foreach ($errors as $key => $value) {
                        echo $value;
                    }
                }
           ?>
           
        </div>

    </div>


</body>
</html>