<?php
    session_start();

    $_SESSION['vali_email']='Bitte wählen Sie';
    $_SESSION['loggedin']=true;

    if (isset($_GET['theme']))
    {
        //Validierung nicht vergessen
        $theme = $_GET['theme'];
        setcookie('themecookie', $theme, time()+60*60*24*365);
    }
    elseif(isset($_COOKIE['themecookie'])){
        $theme = $_COOKIE['themecookie'];
    }
    else{
        $theme = 'light';
    }

    
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cookie-Demo</title>
    <?php
        if($theme == 'dark')
        {
            echo '<link href="css/dark.css" rel="stylesheet">';
        }
        else{
            echo '<link href="css/light.css" rel="stylesheet">';
        }
    ?>
</head>
<body>
    <a href="?theme=dark" class="href">Dark Theme</a>
    <a href="?theme=light" class="href">Light Theme</a>
</body>
</html>