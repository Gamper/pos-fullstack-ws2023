<?php

class User{
    private int $uid;
    private string $uemail;
    private string $upwhash;
    private string $uvorname;
    private string $unachname;

    public function __construct($uemail, $vorname, $nachname, $pwhash)
    {
        //$this->uemail = $uemail;
        $this->setUemail($uemail);
        $this->setUpwhash($pwhash);
        $this->setUnachname($nachname);
        $this->setUvorname($vorname);
    }

    public static function getUserById(int $id)
    {
        //returns one User or Exception
    }


    /**
     * Get the value of uid
     *
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * Set the value of uid
     *
     * @param int $uid
     *
     * @return self
     */
    public function setUid(int $uid): self
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * Get the value of uemail
     *
     * @return string
     */
    public function getUemail(): string
    {
        return $this->uemail;
    }

    /**
     * Set the value of uemail
     *
     * @param string $uemail
     *
     * @return self
     */
    public function setUemail(string $uemail): self
    {
        $this->uemail = $uemail;

        return $this;
    }

    /**
     * Get the value of upwhash
     *
     * @return string
     */
    public function getUpwhash(): string
    {
        return $this->upwhash;
    }

    /**
     * Set the value of upwhash
     *
     * @param string $upwhash
     *
     * @return self
     */
    public function setUpwhash(string $upwhash): self
    {
        $this->upwhash = $upwhash;

        return $this;
    }

    /**
     * Get the value of uvorname
     *
     * @return string
     */
    public function getUvorname(): string
    {
        return $this->uvorname;
    }

    /**
     * Set the value of uvorname
     *
     * @param string $uvorname
     *
     * @return self
     */
    public function setUvorname(string $uvorname): self
    {
        $this->uvorname = $uvorname;

        return $this;
    }

    /**
     * Get the value of unachname
     *
     * @return string
     */
    public function getUnachname(): string
    {
        return $this->unachname;
    }

    /**
     * Set the value of unachname
     *
     * @param string $unachname
     *
     * @return self
     */
    public function setUnachname(string $unachname): self
    {
        $this->unachname = $unachname;

        return $this;
    }
}