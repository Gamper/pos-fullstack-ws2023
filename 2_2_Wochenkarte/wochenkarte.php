<?php
require_once "models/CookieHelper.php";

//Wenn das Consent-Cookie nicht gesetzt wurde, leite auf Startseite um

//Hole die aktuelle Session
session_start();

require_once "models/User.php";


if (User::isLoggedIn() == false)
{
    header('Location: index.php');
    exit();
}

//Wenn der User nicht eingeloggt ist, leite auf Startseite um

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <title>Wochenkarte</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/wochenkarte.css" rel="stylesheet">
</head>
<body class="wochenkarte">

<div class="container">
    <div class="row">
        <div class="col-md-12"><a href="logout.php">Logout</a></div>
    </div>
</div>

<h1>Wochenkarte</h1>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-6 speisen">
            <label>Montag</label>
            <img src="images/monday.jpeg">
        </div>
        <div class="col-md-4 col-sm-6 speisen">
            <label>Dienstag</label>
            <img src="images/tuesday.jpeg">
        </div>
        <div class="col-md-4 col-sm-6 speisen">
            <label>Mittwoch</label>
            <img src="images/wednesday.jpeg">
        </div>
        <div class="col-md-4 col-sm-6 speisen">
            <label>Donnerstag</label>
            <img src="images/thursday.jpeg">
        </div>
        <div class="col-md-4 col-sm-6 speisen">
            <label>Freitag</label>
            <img src="images/friday.jpeg">
        </div>
        <div class="col-md-4 col-sm-6 speisen">
            <label>Samstag</label>
            Ruhetag
        </div>
    </div>
</div> <!-- /container -->

</body>
</html>
