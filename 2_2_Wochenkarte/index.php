<?php
require_once "models/CookieHelper.php";

//Checke, ob das Consent-Cookie gesetzt wurde. Wenn ja, starte die Session
if (CookieHelper::checkCookiesAllowed())
{
    session_start();
}

//Wenn das Cookie-Fomular abgeschickt wurde, setzte das Cookie und lade die Seite neu
if (isset($_POST['cookie_submit']))
{
    CookieHelper::allowCookies();
}


require_once "models/User.php";
//Wenn Login-Formular abgeschickt wurde...
 $user = new User("test@test.de", "testi", "tester", "testitesthash");

if (isset($_POST['submit'])) {
    //.. nimm die Formulardaten und suche damit einen passenden User.
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $pw = isset($_POST['password']) ? $_POST['password'] : '';
    
    //Wenn ein User gefunden wurde, logge diesen ein und leite auf die Wochenkarte um
    if(User::get($email) != false)
    {
        $user = User::get($email);
        $user->login($pw);
        header('Location: wochenkarte.php');
    }
    else{
        //Umleitung auf index.php
        header('Location: index.php');
        exit();
    }
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <title>Anmeldung Wochenkarte</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/wochenkarte.css" rel="stylesheet">
</head>
<body>



<h1>Wochenkarte</h1>

<div class="container">

    <?php

    //Wenn das Cookie nicht gesetzt ist, zeige das Cookie-Form an


    if (!CookieHelper::checkCookiesAllowed())
    {
    ?>
            <form class="form-cookies" action="index.php" method="post">
                <h3>Willkommen</h3>
                <p>Diese Website verwendet Cookies.</p>
                <button name="cookie_submit" class="btn btn-lg btn-warning btn-block" type="submit">Akzeptieren</button>
            </form>
    <?php
    }
    //..sonst zeige das Login-Form an...
    else{
    ?>

        <form class="form-signin" action="index.php" method="post">
            <h3 class="form-signin-heading">Bitte anmelden</h3>

            <label for="inputEmail" class="sr-only">E-Mail</label>
            <input type="email" name="email" class="form-control" placeholder="E-Mail" value="<?= $user->getEmail() ?>"
                   required autofocus>
      
            <?php
            // if (isset($user->getErrors()['email'])) {
            //     echo "<div class='error'>" . $user->getErrors()['email'] . "</div>";
            // } else {
            //     echo "<div>&nbsp;</div>";
            // }
            // ?>
            <br />
            <label for="inputPassword" class="sr-only">Passwort</label>
            <input type="password" name="password" class="form-control" placeholder="Passwort"
                   value="<?= $user->getPassword() ?>" required>
        
            <?php
            // if (isset($user->getErrors()['password'])) {
            //     echo "<div class='error'>" . $user->getErrors()['password'] . "</div>";
            // } else if (isset($user->getErrors()['credentials'])) {
            //     echo "<div class='error'>" . $user->getErrors()['credentials'] . "</div>";
            // } else {
            //     echo "<div>&nbsp;</div>";
            // }
            ?>

            <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Anmelden</button>
        </form>
        
    <?php
        } //Ende Else
    ?>
</div> <!-- /container -->

</body>
</html>
