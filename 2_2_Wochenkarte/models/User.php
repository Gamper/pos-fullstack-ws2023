<?php
require_once 'Database.php';
class User
{
    private $id;
    private $email;
    private $password;

    public function __construct($email, $password)
    {
        $this->setEmail($email);
        $this->setPassword($password);
    }

    public static function get($email)
    {
        $db = Database::connect();
        $stmt = $db->prepare('SELECT * FROM user WHERE email = :email');
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $result = $stmt->fetch();
        if($result)
        {
            return new User($result['email'], $result['password']);
        }
        else{
            return false;
        }
    }

    public function create()
    {
        $db = Database::connect();
        $stmt = $db->prepare('INSERT INTO user (email, password) VALUES (:email, :password)');
        $email = $this->getEmail();
        $stmt->bindParam(':email', $email);
        $passwort_hash = password_hash($this->getPassword(), PASSWORD_DEFAULT);
        $stmt->bindParam(':password', $passwort_hash);
        $stmt->execute();
    }

    public function update()
    {
        $db = Database::connect();
        $stmt = $db->prepare('UPDATE user SET email = :email, password = :password WHERE email = :email');
        $email = $this->getEmail();
        $stmt->bindParam(':email', $email);
        $passwort_hash = password_hash($this->getPassword(), PASSWORD_DEFAULT);
        $stmt->bindParam(':password', $passwort_hash);
        $stmt->execute();
    }

    public function save()
    {
        if($this->get($this->getEmail()))
        {
            echo 'UPDATED';
            $this->update();
        }
        else
        {
            echo 'CREATED';
            $this->create();
        }
    }


    public function login($pw)
    {
        if(password_verify($pw, $this->getPassword()))
        {
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $this->getEmail();
        }
    }

    
    public static function logout()
    {
        $_SESSION['loggedin'] = false;
        session_destroy();
    }


    public static function isLoggedIn()
    {
        if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
        {
            return  true;
        }
        return false;
    }


    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }
}

?>
