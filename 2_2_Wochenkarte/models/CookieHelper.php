<?php


class CookieHelper
{
    /**
     * Methode zum 
     * @return boolean true, wenn das Cookie gesetzt ist, sonst false
     */
    public static function checkCookiesAllowed() {
        if (isset($_COOKIE['consentcookie']) && $_COOKIE['consentcookie'] == 1)
        {
            return true;
        }
        return false; //Todo
    }

    /**
     * Setze das Cookie
     */
    public static function allowCookies() {
        //Todo
        setcookie('consentcookie', true, time()+60*60*24*365);
    }

}
