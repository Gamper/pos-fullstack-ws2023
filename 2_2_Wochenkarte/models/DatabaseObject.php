<?php

require_once 'Database.php';

abstract class DatabaseObject
{

    protected int $id;
    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public abstract function create();

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public abstract function update();

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public abstract static function get($id);

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public abstract static function getAll();

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public abstract static function delete($id);

    public function save()
    {
        if (isset($this->id)) {
            return $this->update();
        } else {
            return $this->create();
        }
    }

    /**
     * Get the value of id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
}