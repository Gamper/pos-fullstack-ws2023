<?php
require_once "models/CookieHelper.php";

session_start();    // session_start has to be the first output


//Logge User über seine Methode logout aus und leite auf die Startseite um
require_once "models/User.php";
session_destroy();
header('Location: index.php');


?>
