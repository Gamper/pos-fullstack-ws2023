<?php

    require_once 'RESTController.php';
    require_once 'models/Station.php';
    require_once 'models/Measurement.php';

    class MeasurementRESTController extends RESTController
    {
        function handleRequest()
        {
            if ($this->method == 'GET' && sizeof($this->args) == 0) {
                $measurements = Measurement::getAll();
                $this->response($measurements, 200);
            }
            elseif ($this->method == 'GET' && sizeof($this->args) == 1 && is_numeric($this->args[0])) {
                $measurement = Measurement::get($this->args[0]);
                $this->response($measurement, 200);
            }
            // elseif($this->method == 'GET' && sizeof($this->args) == 2 && is_numeric($this->args[0]) && $this->args[1] == 'measurements')
            // {
            //     $station = Station::get($this->args[0]);
            //     // $measurements = $station->getMeasurements();
            //     $measurements = Measurement::getAllByStation($this->args[0]);
            //     $this->response($measurements, 200);
            // }
            elseif($this->method == 'POST')
            {
                $this->handlePOSTRequest();
            }
            elseif($this->method == 'PUT' && sizeof($this->args) == 1 && $this->verb == null && is_numeric($this->args[0]))
            {
                $this->handlePUTRequest($this->args[0]);
            }
            elseif($this->method == 'DELETE' && sizeof($this->args) == 1 && $this->verb == null && is_numeric($this->args[0]))
            {
                $this->handleDELETERequest();
            }
            else {
                $this->response('Method not allowed', 405);
            }

            // 
           
            //Entscheide, welche HTTP-Methode verwendet wurde
            //Schau die URL an und...
            //Rufe die richtigen CRUD-Methoden vom Model auf
        }


        private function handleDELETERequest()
        {
            //Lösche Measurement und gib Status 200 zurück
            try{
                $station = Measurement::get($this->args[0]);
                if ($station->delete())
                {
                    $this->response('Measurement deleted', 200);
                }
                else
                {
                    $this->response('Measurement not deleted: Internal server error', 500);
                }
            }
            catch(Exception $e)
            {
                $this->response('Measurement not found', 404);
            }
        }

        private function handlePUTRequest($id)
        {
            //Aktualisiere Measurement und gib Status 200 zurück
            try{
                $measurement = Measurement::get($id);
                $measurement->setTime($this->getDataOrNull('timestamp'));
                $measurement->setTemperature($this->getDataOrNull('temperature'));
                $measurement->setRain($this->getDataOrNull('rain'));
                $station = Station::get($this->getDataOrNull('station_id'));
                $measurement->setStation($station);
                $measurement->setStationId($this->getDataOrNull('station_id'));
                if ($measurement->save())
                {
                    $this->response($measurement, 200);
                }
                else
                {
                    $this->response('Measurement not saved: Internal server error', 500);
                }
            }
            catch(Exception $e)
            {
                $this->response('Measurement not found', 404);
            }
        }

        private function handlePOSTRequest()
        {
            //Erzeuge neue Station und gib diese mit Status 201 zurück
            $newMeasurement = new Measurement();
            $newMeasurement->setTime($this->getDataOrNull('time'));
            $newMeasurement->setTemperature($this->getDataOrNull('temperature'));
            $newMeasurement->setRain($this->getDataOrNull('rain'));
            $station = Station::get($this->getDataOrNull('station_id'));
            $newMeasurement->setStation($station);
            $newMeasurement->setStationId($this->getDataOrNull('station_id'));
            if ($newMeasurement->save())
            {
                $this->response($newMeasurement, 201);
            }
            else
            {
                $this->response('Bad Request', 400);
            }
        }
    }
