<?php

    require_once 'RESTController.php';
    require_once 'models/Station.php';
    require_once 'models/Measurement.php';

    class StationRESTController extends RESTController
    {
        function handleRequest()
        {
            if ($this->method == 'GET') {
                $this->handleGETRequest();
            }
            elseif($this->method == 'POST')
            {
                $this->handlePOSTRequest();
            }
            elseif($this->method == 'PUT' && sizeof($this->args) == 1 && $this->verb == null && is_numeric($this->args[0]))
            {
                $this->handlePUTRequest($this->args[0]);
            }
            elseif($this->method == 'DELETE' && sizeof($this->args) == 1 && $this->verb == null && is_numeric($this->args[0]))
            {
                $this->handleDELETERequest($this->args[0]);
            }
            else {
                $this->response('Method not allowed', 405);
            }

            // 
           
            //Entscheide, welche HTTP-Methode verwendet wurde
            //Schau die URL an und...
            //Rufe die richtigen CRUD-Methoden vom Model auf
        }


        private function handleGETRequest()
        {
            //Gib Station mit ID zurück
            try{
                if ($this->method == 'GET' && sizeof($this->args) == 0) {
                    $stations = Station::getAll();
                    $this->response($stations, 200);
                }
                elseif ($this->method == 'GET' && sizeof($this->args) == 1 && is_numeric($this->args[0])) {
                    $station = Station::get($this->args[0]);
                    $this->response($station, 200);
                }
                elseif($this->method == 'GET' && sizeof($this->args) == 2 && is_numeric($this->args[0]) && $this->args[1] == 'measurements')
                {
                    $station = Station::get($this->args[0]);
                    // $measurements = $station->getMeasurements();
                    $measurements = Measurement::getAllByStation($this->args[0]);
                    $this->response($measurements, 200);
                }
            }
            catch(Exception $e)
            {
                $this->response('Station not found', 404);
            }
        }

        private function handleDELETERequest($id)
        {
            //Lösche Station und gib Status 200 zurück
            try{

                if (Station::delete($id))
                {
                    $this->response('Station deleted', 200);
                }
                else
                {
                    $this->response('Station not deleted: Internal server error', 500);
                }
            }
            catch(Exception $e)
            {
                $this->response('Station not found', 404);
            }
        }

        private function handlePUTRequest($id)
        {
            //Aktualisiere Station und gib Status 200 zurück
            try{
                $station = Station::get($id);
                $station->setName($this->getDataOrNull('name'));
                $station->setAltitude($this->getDataOrNull('altitude'));
                $station->setLocation($this->getDataOrNull('location'));
                if ($station->save())
                {
                    $this->response($station, 200);
                }
                else
                {
                    $this->response('Station not saved: Internal server error', 500);
                }
            }
            catch(Exception $e)
            {
                $this->response('Station not found', 404);
            }
        }

        private function handlePOSTRequest()
        {
            //Erzeuge neue Station und gib diese mit Status 201 zurück
            $newStation = new Station();
            $newStation->setName($this->getDataOrNull('name'));
            $newStation->setAltitude($this->getDataOrNull('altitude'));
            $newStation->setLocation($this->getDataOrNull('location'));
            if ($newStation->save())
            {
                $this->response($newStation, 201);
            }
            else
            {
                $this->response('Bad Request', 400);
            }
        }
    }
