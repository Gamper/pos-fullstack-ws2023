<?php
    
    //ROUTING
    //Todo: Entscheide, welcher Controller benötigt wird.
    //Schau die URL an..
    $route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['stations'];
    $controller = sizeof($route) > 0 ? $route[0] : 'stations';
    
    if ($controller == 'stations') {
        //echo "Lade StationRESTController";
        require_once 'controllers/StationRESTController.php';
        try {
            ($controller = new StationRESTController())->handleRequest();
        } catch(Exception $e) {
            RESTController::responseHelper($e->getMessage(), $e->getCode());
        }
    }
    elseif ($controller == 'measurements') {
        // echo "Lade MeasurementRESTController";
        require_once 'controllers/MeasurementRESTController.php';
        try {
            ($controller = new MeasurementRESTController())->handleRequest();
        } catch(Exception $e) {
            RESTController::responseHelper($e->getMessage(), $e->getCode());
        }
    }
    else {
        RESTController::responseHelper('REST-Controller "' . $controller . '" not found', '404');
    }
    
?>

