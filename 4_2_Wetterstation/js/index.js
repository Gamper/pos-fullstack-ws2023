let chart;

//Deklaration einer asynchronen Funktion zum Holen der Daten
async function getMeasurements()
{
    let id = document.querySelector('#station_dropdown').value;
    let response = await fetch(`api/stations/${id}/measurements`);
    let jsonresponse = await response.json();
    console.log(jsonresponse);
    let temperatureList = [];
    let rainList = [];
    let timestampList = [];
    for (const element of jsonresponse) {
        console.log(element.temperature);
        temperatureList.push(element.temperature);
        timestampList.push(element.time);
        rainList.push(element.rain*100);
    }
    console.log(rainList);
    // console.log(timestampList);
    createChart(temperatureList, timestampList, rainList);
    //Hier wird der DOM manipuliert
}

//(Neu-)Erzeugen des Charts
function createChart(tempdata, timedata, raindata)
{
    const canvasEl = document.getElementById("chart");
    console.log(canvasEl);
    //Chart neu aufbauen oder zerstören und neu erstellen
    if(chart)
    {
        chart.destroy();
    }
    
    chart = new Chart(canvasEl, {
            // The type of chart we want to create
            type: 'line',
    
            // The data for our dataset
            data: {
                labels: timedata,
                datasets: [
                    {
                        label: "Temperatur [°C]",
                        data: tempdata,
                        borderColor: 'rgb(75, 192, 192)',
                        backgroundColor: 'rgb(75, 192, 192)',
                        fill: false,
                        tension: 0
                    },
                    {
                        label: "Regenwahrscheinlichkeit [%]",
                        data: raindata,
                        borderColor: 'rgb(20,170, 70)',
                        backgroundColor: 'rgb(20,170, 70)',
                        fill: false,
                        tension: 0
                    }
                ]
            },
    
            // Configuration options go here
            options: {
                // scales: {
                //     yAxes: [{
                //         // type: 'line',
                //         // position: 'left',
                //         // ticks: {
                //         //     beginAtZero: true,
                //         //     max: 25
                //         // }
                //     }]
                // }
            }
        });//Ende Chart
}//Ende createChart


document.addEventListener("DOMContentLoaded", function(){

    let buttonEl = document.getElementById("btnSearch");
    buttonEl.onclick = getMeasurements;

    getMeasurements();

    // buttonEl.addEventListener("click", (element) => {console.log(element);});

})