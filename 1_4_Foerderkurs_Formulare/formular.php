<?php
    // var_dump($_POST);
    //1. Schritt: Daten entgegennehmen
    //$email = isset($_POST["email"]) ? $_POST["email"] :"";
    if (isset($_POST['email']) && checkEmail($_POST['email'])) 
    {
        $email = $_POST['email'];

        //3. Schritt: Mit den Daten arbeiten: Ausgeben, in DB speichern, ...
        echo "Validierung der Email hat funktioniert: ";
        echo htmlspecialchars($email);

        //2. Schritt: Validieren der Form-Daten
        // filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        //var_dump(checkEmail($_POST['email']));
    }
    else
    {
        //Todo: Fehlermeldungen sammeln
        $email = "";
        echo "Validierung der Email hat NICHT funktioniert: ";
        echo htmlspecialchars($email);
    }

    function checkEmail($emailCandidate)
    {
        // preg_match()
        // var_dump($emailCandidate);
        $pattern = '/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/m';
        
        if (preg_match($pattern, $emailCandidate) === 1)
        {
            return true;
        }
        return false;
    }
    
    
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.css" rel="stylesheet">
    <script src="js/formular.js"></script>
    <title>Registrierung</title>
</head>
<body>
<form action="" method="post" class="container">
  <div class="form-example">
    <label for="name" class="form-label">Vorname: </label>
    <input class="form-control" type="text" name="vorname" id="name" placeholder="Maxine" required />
  </div>
  <div class="form-example">
    <label for="nname" class="form-label">Nachname: </label>
    <input class="form-control" type="text" name="nachname" id="nname" placeholder="Musterfrau" required />
  </div>
  <div class="form-example">
    <label for="email" class="form-label">Enter your email: </label>
    <input class="form-control" type="text"  name="email" id="email" />
    <!-- TODO:  value="<?php if (isset($_POST['email'])) {echo htmlspecialchars($_POST['email']);} ?>" -->
  </div>
  <div class="form-example">
    <label for="date" class="form-label">Geburtsdatum: </label>
    <input class="form-control" type="date" name="gebdatum" id="date" required />
  </div>
  <div class="form-example">
    <input class="btn btn-danger" type="submit" value="Registrieren" />
  </div>
</form>

</body>
</html>