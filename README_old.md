
---

>"JavaScript wird sich als Programmiersprache nie durchsetzen." [GAMP 2010]

---

# Mitschrift POS Wintersemester 2023

In der Mitschrift werden wichtige Unterrichtskonzepte notiert, ebenso während der Bearbeitung der Arbeitsaufträge auftauchende Fragen.


## Übersicht der Projekte

* [Startseite](index.html)
* [0_HalloPHP](0_hellophp/index.php)
* [0_Registrierung](0_hellophp/register_form.html)
* [1_1_Notenerfassung](1_1_Notenerfassung/index.php)
* [1_2_BMI-Rechner](1_2_BMIRechner_Demo/index.php)


## 19.9.2023: Einleitung PHP  - Teil 1
Kurze Einleitung in die Konzepte von PHP als Server-seitige Skriptsprache auf der Command Line und als Interpreter für eingebetteten Code in HTML-Dokumenten. 

![Überblick](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/PHP_funktionsweise.svg/780px-PHP_funktionsweise.svg.png?20121129070241)

Tags: HTTP-Request-Response, dynamische Inhalte mit serverseitiger Programmierung

Besprochene Konzepte:
- Funktionsweise von PHP als server-seitige Skriptsprache
- Variablen (Definition, Wertzuweisung, Literalschreibweisen, Variablensubstitution) und Konstanten
- Vordefinierte (superglobale) Variablen
- Schwache, dynamische Typisierung von PHP. Optionale Typangaben. Type Juggling.
- Fertige Funktionen verwenden (Doku lesen, call-by-value vs. call-by-reference)
- Eigene Funktionen definieren und aufrufen
- Einfache Klassen definieren und verwenden
- Standard-konforme Dokumentation schreiben
- Indizierte und assoziative Arrays verwenden

````php
//Definieren von Variablen
$name = 'Michael';
echo "Username $name";

//Definition Mit Datentyp
String $nachname = 'Gamper';

/**
 * Diese Funktion bildet aus einem Vornamen und einem Nachnamen einen String
 * @param String Vorname
 * @param String Nachname
 * @param String Anrede Optionaler Parameter. Wenn nicht angegeben, ist die Anrede 'Herr'
 * @return String Fullname
 */
function getFullname($vname, $nname, $anrede='Herr')
{
    //TODO: Parameter validieren
    
    return $anrede .' ' . $vname.' '.$nname;
}

//Aufruf der Funktion
getFullname('Michael','Gamper')

````

## 26.9.2023: Einleitung PHP  - Teil 2

*Programm*
- Formularverarbeitung
- Datenübermittlung mit HTTP GET und POST
- Validierung am Client
- Validierung am Server

*Softskills*
- Responsive Design mit Bootstrap
- Versionsverwaltung und Kollaboration mit GIT
- Server on demand mit Docker
- Textauszeichnung mit MarkDown
- VSCode Shortcuts

## 24.10.2023: Theorie und Demo BMI-Rechner

## 07.11.2023: BMI-Rechner


