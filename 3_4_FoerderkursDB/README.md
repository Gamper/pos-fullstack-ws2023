
# Vorbereitungseinheit POS-PLF3


Wichtige Konzepte:
* CRUD-Implementierung 
* DB-Zugriff: Prepared Statements
* ORM
* OO Design Patterns: Active Record
* DB-Verwaltung mit PHPMyAdmin

Fragen:
* Dropdown: HTML vorgegeben -> Model-Methode richtig aufrufen
* ORM: automatisch vs. händisch 

### Active Record Pattern und ORM

Das Interface gibt die sogenannten CRUD-Methoden vor, die alle Klassen implementieren müssen. Der Vorteil: Client-Code, der diese Model-Klassen verwendet, kann immer dieselben Methoden aufrufen, egal in welchem Kontext. Der Client muss sich nicht um SQL kümmern, sondern kann mit Objekten arbeiten (Objekt-relationales Mapping).


````mermaid
---
title: Acitve Record Pattern
---
classDiagram
    ActiveRecord <|-- Kunde 
    ActiveRecord <|-- Bestellung 

    class ActiveRecord
    <<interface>> ActiveRecord
    ActiveRecord : create()
    ActiveRecord : update()
    ActiveRecord : get()$
    ActiveRecord : getAll()$
    ActiveRecord : delete()$

    class Kunde{
        +String vorname
        +String ...
        +getVorname() String
        +get...() String
    }

    class Bestellung{
        +String ...
        +get...() String
    }

````

### Prepared Statements
Prepared Statements werden von allen gängigen Programmiersprachen und Datenbank-System unterstützt. Der Ablauf:
* Client (z.B. PHP) baut die Datenbankverbindung auf.
* Client schickt eine SQL-Anweisung mit Platzhaltern statt konkreten Werten.
* DB-Server (z.B. MySQL) prüft den SQL-Syntax und bereitet die beteiligten Tabellen vor und meldet den Erfolg an den Client.
* Client bindet Werte an die Platzhalter über Typisierung und schickt die Werte an den DB-Server.
* DB-Server setzt die Werte ein und liefert das Ergebnis zurück.

Vorteile:
* Performance: Durch das Vorbereiten und Prüfen können ähnliche DB-Abfragen schneller ablaufen, indem einfach die neuen Werte eingesetzt werden.
* Sicherheit: Inputs durch den Client werden an Datentypen gebunden. So werden selbst bösartige SQL-Befehle aus Formulareingaben nur als Werte interpretiert und nie ausgeführt. (Schutz gegen SQL-Injections),


### Beispiel Content Management System (CMS)

````mermaid

    classDiagram

    Database <-- ActiveRecord
    ActiveRecord  <|-- Beitrag   : implements
    ActiveRecord  <|-- User   : implements
    Beitrag *--> User

    class Database
    Database : connect()$
    Database : disconnect()$

    class ActiveRecord
    <<interface>> ActiveRecord
    ActiveRecord : create()
    ActiveRecord : update()
    ActiveRecord : get()$
    ActiveRecord : getAll()$
    ActiveRecord : delete()$

    class Beitrag {
        -int id
        -String titel
        -String inhalt
        -User besitzer
        -DateTime freigabedatum
        +getOwner() User
        -setOwner(newOwner User)
    }

    class User {
        -int uid
        -String uname
        -String uemail
        -String upwhash
    }
````

