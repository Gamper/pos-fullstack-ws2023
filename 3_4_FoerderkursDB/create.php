
<?php
    //NUR ein kleines Testskript zum Testen der Klasse Kunde

    require 'models/Kunde.php';
    
    //Todo: restliche Formulardaten erheben
    if(isset($_POST['vorname']) && isset($_POST['nachname']) && isset($_POST['geburtsdatum']))
    {
        $kunde = new Kunde();
        $kunde->setVorname($_POST['vorname']);
        $kunde->setNachname($_POST['nachname']);
        $kunde->setGeburtsdatum($_POST['geburtsdatum']);
        if($kunde->validate())
        {
            $kunde->create();
        }
      
    }
    else
    {
        echo 'Bitte füllen Sie alle Felder aus';
    }


?>



<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <select>
            <?php
                $kunden = Kunde::getAll();
  
                foreach(Kunde::getAll() as $kunde)
                { 
                    echo '<option>'.htmlspecialchars($kunde->getVorname()).'</option>';
                }
            ?>
        </select>
        <!-- Todo: alle Kundendaten erheben -->
        <form action="create.php" method="POST">
            <input type="text" name="vorname" placeholder="Vorname">
            <input type="text" name="nachname" placeholder="Nachname">
            <input type="text" name="geburtsdatum" placeholder="Geburtsdatum">
            <button type="submit">Kunde neu anlegen</button>
        </form>
    </body>
    </html>