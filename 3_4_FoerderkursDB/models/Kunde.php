<?php
require_once('DatabaseObject2.php');

class Kunde implements DatabaseObject2
{
    private $kundenid;
    private $vorname;
    private $nachname;
    private $geburtsdatum;
    private $telefonnummer;
    private $strasse;
    private $hausnummer;
    private $plz;
    private $ort;
    private $land;

    // public function __construct()
    // {
        
    // }
    
    public static function getAll()
    {
        try{
            //Verbindung aufbauen
            $db = Database::connect();
            $sql = "SELECT * FROM kunden LIMIT ?,?";
            //Prepare Statement
            $stmt = $db->prepare($sql);
            $start = 0;
            $anzahl = 10;
            $stmt->bindValue(1, $start, PDO::PARAM_INT);
            $stmt->bindValue(2, $anzahl, PDO::PARAM_INT);
            //Statement ausführen
            $stmt->execute();
            //Ergebnis abholen
            // $result = $stmt->fetchAll(PDO::FETCH_CLASS, 'Kunde');
            //Mappen
            $result = $stmt->fetchAll();
            $alleKunden = [];
            foreach($result as $key => $value)
            {
                $kundenobjekt = new Kunde();
                $kundenobjekt->setVorname($value['Vorname']);
                $kundenobjekt->setNachname($value['Nachname']);
                $kundenobjekt->setGeburtsdatum($value['Geburtsdatum']);
                $kundenobjekt->setTelefonnummer($value['Telefonnummer']);
                $kundenobjekt->setStrasse($value['Strasse']);
                $kundenobjekt->setHausnummer($value['Hausnummer']);
                $kundenobjekt->setPlz($value['PLZ']);
                $kundenobjekt->setOrt($value['Ort']);
                $kundenobjekt->setLand($value['Land']);
                $kundenobjekt->setKundenid($value['KundenID']);
                array_push($alleKunden, $kundenobjekt);
            }
            return $alleKunden;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        
       
        

    }



    public function create()
    {
        try{
            $db = Database::connect();
            $query = "INSERT INTO kunden (Vorname, Nachname, Geburtsdatum, Telefonnummer, Strasse, Hausnummer, PLZ, Ort, Land) VALUES (?,?,?,?,?,?,?,?,?);";
            $stmt = $db->prepare($query);
            $stmt->execute([$this->vorname, $this->nachname, $this->geburtsdatum, $this->telefonnummer, $this->strasse, $this->hausnummer, $this->plz, $this->ort, $this->land]);
            $id = $db->last_insert_id();
            $this->setKundenid($id);
            return $this;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function update() {}

    public static function get($id) {}

    public static function delete($id) {}


    public function validate()
    {
        //Todo: Validieren
        return true;
    }

    public function getVorname()
    {
        return $this->vorname;
    }

    /**
     * Set the value of vorname
     */
    public function setVorname($vorname): self
    {
        $this->vorname = $vorname;

        return $this;
    }

    /**
     * Get the value of kundenid
     */
    public function getKundenid()
    {
        return $this->kundenid;
    }

    /**
     * Set the value of kundenid
     */
    public function setKundenid($kundenid): self
    {
        $this->kundenid = $kundenid;

        return $this;
    }

    /**
     * Get the value of nachname
     */
    public function getNachname()
    {
        return $this->nachname;
    }

    /**
     * Set the value of nachname
     */
    public function setNachname($nachname): self
    {
        $this->nachname = $nachname;

        return $this;
    }

    /**
     * Get the value of geburtsdatum
     */
    public function getGeburtsdatum()
    {
        return $this->geburtsdatum;
    }

    /**
     * Set the value of geburtsdatum
     */
    public function setGeburtsdatum($geburtsdatum): self
    {
        $this->geburtsdatum = $geburtsdatum;

        return $this;
    }

    /**
     * Get the value of telefonnummer
     */
    public function getTelefonnummer()
    {
        return $this->telefonnummer;
    }

    /**
     * Set the value of telefonnummer
     */
    public function setTelefonnummer($telefonnummer): self
    {
        $this->telefonnummer = $telefonnummer;

        return $this;
    }

    /**
     * Get the value of strasse
     */
    public function getStrasse()
    {
        return $this->strasse;
    }

    /**
     * Set the value of strasse
     */
    public function setStrasse($strasse): self
    {
        $this->strasse = $strasse;

        return $this;
    }

    /**
     * Get the value of hausnummer
     */
    public function getHausnummer()
    {
        return $this->hausnummer;
    }

    /**
     * Set the value of hausnummer
     */
    public function setHausnummer($hausnummer): self
    {
        $this->hausnummer = $hausnummer;

        return $this;
    }

    /**
     * Get the value of plz
     */
    public function getPlz()
    {
        return $this->plz;
    }

    /**
     * Set the value of plz
     */
    public function setPlz($plz): self
    {
        $this->plz = $plz;

        return $this;
    }

    /**
     * Get the value of ort
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * Set the value of ort
     */
    public function setOrt($ort): self
    {
        $this->ort = $ort;

        return $this;
    }

    /**
     * Get the value of land
     */
    public function getLand()
    {
        return $this->land;
    }

    /**
     * Set the value of land
     */
    public function setLand($land): self
    {
        $this->land = $land;

        return $this;
    }
}

?>