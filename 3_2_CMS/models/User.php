<?php
require_once('DatabaseObject.php');
session_start();

class User extends DatabaseObject{
    private String $uname;
    private String $upwhash;
    private String $uemail;

    //Konstruktor darf hier keine Parameter haben, damit das automatische ORM mit fetchObject und fetchAll funktioniert
    public function __construct()
    {
        
    }


    public static function login($username, $password)
    {
        $db = Database::connect();
        $stmt = $db->prepare('SELECT * FROM tbl_users WHERE uname = :uname');
        $stmt->bindParam(':uname', $username);
        $stmt->execute();
        $user = $stmt->fetchObject('User');
        if ($user) {
            if (password_verify($password, $user->upwhash)) {
                $_SESSION['uid'] = $user->id;
                return true;
            }
        }
        return false;
    }
    

    public static function isLoggedIn()
    {
        //Debuggin enabled (dummy login active)
        $debug = true;
        if ($debug)
        {
            $_SESSION['uid'] = 1;
            $_SESSION['uname'] = 'admin'; 
        }

        if (isset($_SESSION['uid'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getAll()
    {
        $db = Database::connect();
        $stmt = $db->prepare('SELECT * FROM tbl_users');
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_CLASS, 'User');
        return $users;
    }

    public static function get($id)
    {
        $db = Database::connect();
        $stmt = $db->prepare('SELECT * FROM tbl_users WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $user = $stmt->fetchObject('User');
        return $user;
    }

    public function create()
    {
        //TODO: Validation
        //Dummy User Creation activated

        try{
            $db = Database::connect();
            $stmt = $db->prepare('INSERT INTO tbl_users (uname, upwhash, uemail) VALUES (:uname, :upwhash, :uemail)');
            $stmt->bindParam(':uname', $this->uname, PDO::PARAM_STR);
            $stmt->bindParam(':upwhash', $this->upwhash, PDO::PARAM_STR);
            $stmt->bindParam(':uemail', $this->uemail, PDO::PARAM_STR);
            $stmt->execute();
            $this->id = $db->lastInsertId();
        } catch (Exception $e) {
            throw new Exception('User: User could not be created');
        }
     
        
    }

    public function update()
    {
        
    }

    public static function delete($id)
    {
        
    }


    /**
     * Get the value of uname
     *
     * @return String
     */
    public function getUname(): String
    {
        return $this->uname;
    }

    /**
     * Set the value of uname
     *
     * @param String $uname
     *
     * @return self
     */
    public function setUname(String $uname): self
    {
        $this->uname = $uname;

        return $this;
    }

    /**
     * Get the value of upwhash
     *
     * @return String
     */
    public function getUpwhash(): String
    {
        return $this->upwhash;
    }

    /**
     * Set the value of upwhash
     *
     * @param String $upwhash
     *
     * @return self
     */
    public function setUpwhash(String $upw): self
    {
        $this->upwhash = password_hash($upw, PASSWORD_DEFAULT);
        return $this;
    }

    /**
     * Get the value of uemail
     *
     * @return String
     */
    public function getUemail(): String
    {
        return $this->uemail;
    }

    /**
     * Set the value of uemail
     *
     * @param String $uemail
     *
     * @return self
     */
    public function setUemail(String $uemail): self
    {
        $this->uemail = $uemail;

        return $this;
    }


}
    
