<?php
require_once 'User.php';
require_once 'DatabaseObject.php';
class Article extends DatabaseObject{
    private string $atitle;
    private string $atext;
    private string $valid_from;
    private User $author;

    public function __construct()
    {
        
    }

    /**
     * Get all objects from the database
     * @return array of nice Article objects
     * @todo Exception handling
     */
    public static function getAll()
    {
        $db = Database::connect();
        $stmt = $db->prepare('SELECT * FROM tbl_articles');
        $stmt->execute();
        //$articles = $stmt->fetchAll(PDO::FETCH_CLASS, 'Article');
        $articles = $stmt->fetchAll(PDO::FETCH_CLASS);
        $realArticlesObjects = [];
        foreach ($articles as $article_as_stdObject) {
            $article = new Article();
            $article->setId($article_as_stdObject->id);
            $article->setAtitle($article_as_stdObject->atitle);
            $article->setAtext($article_as_stdObject->atext);
            $article->setValidFrom($article_as_stdObject->valid_from);
            //Hole über den Fremdschlüssel im Artikel den Autor
            $user = User::get($article_as_stdObject->uid);
            $article->setAuthor($user);
            $realArticlesObjects[] = $article;
        }
        return $realArticlesObjects;
    }


    /**
     * Get an Article object by its id
     * @return Article
     */
    public static function get($id)
    {
        try {
            $db = Database::connect();
            $stmt = $db->prepare('SELECT * FROM tbl_articles WHERE id = ? AND name= ?');

            $stmt->execute();
   
            // Statt dem automatischen ORM mit
            // $article = $stmt->fetchObject('Article');
            // Hole ein Standardobjekt von der Datenbank
            $stdObject = $stmt->fetchObject();
            //Mappe das Standardobjekt auf ein Article-Objekt
            $article = new Article();
            $article->setId($stdObject->id);
            $article->setAtitle($stdObject->atitle);
            $article->setAtext($stdObject->atext);
            $article->setValidFrom($stdObject->valid_from);
            //Hole über den Fremdschlüssel im Artikel den Autor
            $user = User::get($stdObject->id);
            //Setze das Autor-Objekt im Artikel
            $article->setAuthor($user);
            return $article;
        } catch (Exception $e) {
            echo $e->getMessage();
            throw new Exception('Article: Fehler beim Holen des Artikels aus der Datenbank!');
        }
      
    }


    /**
     * Create or update an object in the database
     * @return boolean true on success
     * @todo Exception handling
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->getId() > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }


    /**
     * Create a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     * @todo Exception handling
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_articles (uid, atitle, atext, valid_from) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->author->getId(), $this->atitle, $this->atext, $this->valid_from));
        $this->id = $db->lastInsertId();
        Database::disconnect();
        // var_dump($this->id);
        return $this->id;
    }


    /**
     * Udpates the object to the database
     * @todo Exception handling
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_articles set uid = ?, atitle = ?, atext = ?, valid_from = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->author->getId(), $this->atitle, $this->atext, $this->valid_from, $this->id));
        Database::disconnect();
    }


    /**
     * Deletes the object from the database by its id
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_articles WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    
    /**
     * Get the value of atitle
     *
     * @return string
     */
    public function getAtitle(): string
    {
        return $this->atitle;
    }

    /**
     * Set the value of atitle
     *
     * @param string $atitle
     *
     * @return self
     */
    public function setAtitle(string $atitle): self
    {
        $this->atitle = $atitle;

        return $this;
    }

    /**
     * Get the value of atext
     *
     * @return string
     */
    public function getAtext(): string
    {
        return $this->atext;
    }

    /**
     * Set the value of atext
     *
     * @param string $atext
     *
     * @return self
     */
    public function setAtext(string $atext): self
    {
        $this->atext = $atext;

        return $this;
    }

    /**
     * Get the value of valid_from
     *
     * @return string
     */
    public function getValidFrom(): string
    {
        return $this->valid_from;
    }

    /**
     * Set the value of valid_from
     *
     * @param string $valid_from
     *
     * @return self
     */
    public function setValidFrom(string $valid_from): self
    {
        $this->valid_from = $valid_from;

        return $this;
    }

    /**
     * Get the value of author
     *
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * Set the value of author
     *
     * @param User $author
     *
     * @return self
     */
    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }

    // public function setId($id)
    // {
    //     parent::setId($id);
    // }

    // public function getId()
    // {
    //     return parent::$id;
    // }



public function validate()
{
    return $this->validateOwner() & $this->validateTitle() & $this->validateContent() & $this->validateReleasedate();
}


private function validateOwner()
    {
        if ($this->author == null || $this->author->getId() <= 0) {
            $this->errors['owner'] = "Besitzer ist ungültig";
            return false;
        } else {
            return true;
        }
    }

    private function validateTitle()
    {
        if (strlen($this->atitle) == 0) {
            $this->errors['title'] = "Titel darf nicht leer sein";
            return false;
        } else if (strlen($this->atitle) > 45) {
            $this->errors['title'] = "Titel zu lang (max. 45 Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    private function validateContent()
    {
        if (strlen($this->atext) == 0) {
            $this->errors['content'] = "Inhalt darf nicht leer sein";
            return false;
        } else {
            return true;
        }
    }

    private function validateReleasedate()
    {
        $d = DateTime::createFromFormat('Y-m-d', $this->valid_from);
        if ($d && $d->format('Y-m-d') == $this->valid_from) {
            return true;
        } else {
            $this->errors['releasedate'] = "Ungültiges Datum";
            return false;
        }
    }

}

?>
