CREATE DATABASE IF NOT EXISTS `cmsDB` /*!40100 DEFAULT CHARACTER SET utf8 */; 
USE `cmsDB`;

CREATE TABLE IF NOT EXISTS tbl_users(
    id INT AUTO_INCREMENT PRIMARY KEY,
    uname VARCHAR(50) NOT NULL,
    upwhash VARCHAR(255) NOT NULL,
    uemail VARCHAR(255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (1, 'John Doe', 'password1', 'john@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (2, 'Jane Smith', 'password2', 'jane@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (3, 'Mike Johnson', 'password3', 'mike@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (4, 'Emily Davis', 'password4', 'emily@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (5, 'David Wilson', 'password5', 'david@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (6, 'Sarah Thompson', 'password6', 'sarah@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (7, 'Michael Brown', 'password7', 'michael@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (8, 'Jessica Lee', 'password8', 'jessica@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (9, 'Christopher Clark', 'password9', 'christopher@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT IGNORE INTO tbl_users (id, uname, upwhash, uemail, created_at, updated_at) VALUES (10, 'Amanda Rodriguez', 'password10', 'amanda@example.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);



CREATE TABLE IF NOt EXISTS tbl_articles(
    id INT AUTO_INCREMENT PRIMARY KEY,
    atitle VARCHAR(255) NOT NULL,
    atext TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    valid_from TIMESTAMP NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    uid INT NOT NULL,
    FOREIGN KEY (uid) REFERENCES tbl_users(id)
);

INSERT IGNORE INTO tbl_articles (aid, atitle, atext, created_at, valid_from, updated_at, uid) VALUES (1, 'Article 1', 'This is article 1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1);
INSERT IGNORE INTO tbl_articles (aid, atitle, atext, created_at, valid_from, updated_at, uid) VALUES (1, 'Article 2', 'This is article 2', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1);