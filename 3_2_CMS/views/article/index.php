<?php

require_once("../../models/User.php");
require_once("../../models/Article.php");

if (!User::isLoggedIn()) {
    header('Location: ../index.php');
    exit();
}

$articles = Article::getAll();

?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <div class="row">
        <h2>Beiträge</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Titel</th>
                <th>Inhalt</th>
                <th>Besitzer</th>
                <th>Freigabedatum</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach ($articles as $article) {
                ?>

                <tr>
                    <td><?= $article->getaTitle() ?></td>
                    <td><?= $article->getaText() ?></td>
                    <td><?= $article->getAuthor()->getUName() ?></td>
                    <td><?= $article->getValidFrom() ?></td>
                    <td><a class="btn btn-info" href="view.php?id=<?= $article->getId() ?>">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                        <a class="btn btn-primary" href="update.php?id=<?= $article->getId() ?>">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a class="btn btn-danger" href="delete.php?id=<?= $article->getId() ?>">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>

                <?php
            }

            ?>

            </tbody>
        </table>
    </div>
</div> <!-- /container -->
</body>
</html>