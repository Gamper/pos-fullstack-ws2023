<?php
session_start();
require_once('models/Book.class.php');
require_once('models/CartItem.class.php');
require_once('models/Cart.class.php');
$book = new Book('1', 'Fullstack Dev Sch...', 5500, 50);

// var_dump($book);

//Testen der CRUD-Methoden
// var_dump(Book::getAll());

// if (Book::get(5) != false)
// {
//     var_dump(Book::get(5));
// }
// else{
//     echo "Buch nicht gefunden";
// }

try {
    // var_dump(Book::get2(5));
    // $item = new CartItem(Book::get(5),2);
    // $cart = new Cart();
    // $cart->add($book, 3);
    // var_dump($cart);
    // $buecher = Book::getAll();
    // foreach ($buecher as $key => $value) {
    //     // echo '<h1>'.$value->getTitle(). '</h1>';
    // }


    // $serialized = serialize($buecher);
    // $deserialized = unserialize($serialized);
    // var_dump($deserialized);
} catch (Exception $th) {
    echo $th->getMessage();
    //Todo: Qualifizierte Fehlermeldung an VIEW übergeben
}







?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta href="light.css">
    <?php

        USER::login($_POST['email'], $_POST['email']);

        USER::isLoggedIn()
        USER::logout();
        // if($_POST['email']=='seppie@test.at' && $_POST['email'] == 'testitest')
        // {
        //     $_SESSION['loggedin'] = true;
        // }

        if(isset($_COOKIE['themecookie']))
        {
            $theme = $_COOKIE['themecookie'];
            if ($theme == 'dark')
            {
                echo '<meta href="dark.css">';
            }
        }

        if(isset($_GET['theme']) && $_GET['theme'] == 'dark')
        {
            
            setcookie('themecookie', 'dark', time()+24*60*60*365);
        }
        else{
            setcookie('themecookie', 'light', time()+24*60*60*365);
            echo '<meta href="light.css">';
        }
    ?>
    
    

    <title>Document</title>
</head>
<body>
    <a href="index.php?theme=light">Light theme</a> 
    <a href="index.php?theme=dark">Dark theme</a> 
</body>
</html>