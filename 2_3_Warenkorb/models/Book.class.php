<?php

class Book
{
    private int $id;
    private string $title;
    private int $price;
    private int $stock;
    

    public function __construct($id, $title, $price, $stock)
    {
        $this->setId($id);
        $this->setTitle($title);
        $this->setPrice($price);
        $this->setStock($stock);
    }


    /**
     * Methode holt alle Bücher aus der Persistenzschicht
     * und gibt diese als Array of Books-Objekt zurück
     * @return array Ein Array von Book-Objekten
     */
    public static function getAll(): array
    {
        $dateiinhalt = file_get_contents('data/PHP-23 bookdata.json');
        //Todo: Fehlerfall behandeln
        $objektliste = json_decode($dateiinhalt);
        $booklist = [];
        foreach ($objektliste as $key => $value) {
            //Mappe ein PHP Standardobjekt zu einem Buch-Objekt
            $book = new Book($value->id, $value->title, $value->price, $value->stock);
            array_push($booklist, $book);
        }
        return $booklist;
    }

    /**
     * Hole ein Buch über seine ID aus der Persistenzschicht
     * Liefert ein Buch-Objekt zurück, oder false falls keines gefunden wurde
     * @param int $id Die ID des gesuchten Buches
     * @return Book|false Liefert das Buch oder false
     */
    public static function get(int $id): Book|false
    {
        $dateiinhalt = file_get_contents('data/PHP-23 bookdata.json');
        $objektliste = json_decode($dateiinhalt);
       
        foreach ($objektliste as $key => $value) {
            if($value->id ==$id)
            {
                //Buch gefunden
                $book = new Book($value->id, $value->title, $value->price, $value->stock);
                return $book;
            }
        }
        return false;
    }


     /**
     * Hole ein Buch über seine ID aus der Persistenzschicht
     * Liefert ein Buch-Objekt zurück, oder false falls keines gefunden wurde
     * @param int $id Die ID des gesuchten Buches
     * @return Book Liefert das Buch oder false
     * @throws Exception Falls kein Buch gefunden wurde
     */
    public static function get2(int $id): Book
    {
        $dateiinhalt = file_get_contents('data/PHP-23 bookdata.json');
        $objektliste = json_decode($dateiinhalt);
       
        foreach ($objektliste as $key => $value) {
            if($value->id ==$id)
            {
                //Buch gefunden
                $book = new Book($value->id, $value->title, $value->price, $value->stock);
                return $book;
            }
        }
        throw new Exception('Book: book not found!');
    }

    


    /**
     * Get the value of id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of price
     *
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * Set the value of price from a string (euros) 
     * stores the value as int (cents)
     *
     * @param string $price potential book price form data source
     * @todo Exception handling. If parameter is not convertable to a number
     * @return self
     */
    public function setPrice(string $price): self
    {
        $this->price = $price*100;

        return $this;
    }

    /**
     * Get the value of stock
     *
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * Set the value of stock
     *
     * @param int $stock
     *
     * @return self
     */
    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }
}

?>