<?php
require_once('Book.class.php');
class CartItem
{
    private Book $book;
    private int $count;

    /**
     * Generates a new CartItem with one book an prefered amount
     * @param Book $book Book Objekt
     * @param int $count The amount of this book in the cart
     */
    public function __construct(Book $book, int $count)
    {
        $this->setBook($book);
        $this->setCount($count);
    }


    /**
     * Get the value of book
     *
     * @return Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * Set the value of book
     *
     * @param Book $book
     *
     * @return self
     */
    public function setBook(Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get the value of count
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * Set the value of count
     *
     * @param int $count
     *
     * @return self
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }
}


?>
