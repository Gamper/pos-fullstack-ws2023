<?php
require_once('CartItem.class.php');
class Cart{
    private array $items;

    public function __construct()
    {
        $this->items = [];
    }

    /**
     * Set the value of items
     *
     * @param CartItem[] $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }


    /**
     * Gets all cart items for this cart
     *
     * @return CardItem[] CardItems im Warenkorb als Array
     */
    public function getItems(): array
    {
        return $this->items;
    }


    /**
     * @param Book $book A book-oject for your cart 
     * @param int $int the desired amount of this book
     */
    public function add(Book $book, int $amount)
    {
        $item = new CartItem($book, $amount);
        //array_push($this->items, $item);
        $this->items[$book] = $amount;
    }


 

}

?>