# Warenkorb-Übung

Fokus der Übung ist das Vertiefen der Konzepte
* Objekt-orientierung in PHP 
* Persistierung mit Cookies und/oder Sessions, JSON

Diese Übung soll der Vorbereitung auf den Test und auf das Thema "Datenbank-Zugriff über ORM" dienen.

Zusätzlich zu den beiden Kernthemen kommen folgende Themen vor:
* Objekt-(relationales) Mapping (ORM)
* CRUD-Metoden: Read aus einer Persistenzschicht
* Beziehungen zwischen Objekten (1:n)
* Union Types und statische Typisierung in PHP >= 8
* Exception Handling (Throwable, Exception, Error) in PHP >= 8

## UML-Klassendiagramm

Der Entwurf zeigt die drei Klassen, deren Zusammenhang und die wichtigsten Methoden. Der Einfachheit halber wurden die standard Getter und Setter sowie triviale Use-Beziehungen wegabstrahiert.

```mermaid

classDiagram

    Cart "1" o--> "0..*" CartItem
    CartItem "1" *--> "1" Book

    class Book{
        -Int bId
        -String bTitle
        -Decimal bPrice
        -Int bStock
        +get(Int id) Book
        +getAll() Book[]
        +...()
    }

    class CartItem{
        +Book book
        +Int amount
         +...()
    }

    class Cart{
        -Array[Book, int] items
        +add(Book book, Int count)
        +remove(Int id)
        +save()
        +load()
        +getTotalPrice() Decimal
        +getItemsCount() Int
         +...()
    }


```