<?php

class Book
{
    private int $id;
    private string $title;
    private float $price;
    private int $stock;


    public function __construct($id, $title, $price, $stock)
    {
        $this->setId($id);
        $this->setTitle($title);
        $this->setPrice($price);
        $this->setStock($stock);
    }


    /*
    * Helper method for loading the raw data
    */
    private static function loadData(): array
    {
        $data = file_get_contents('data/PHP-23 bookdata.json');
        $books = json_decode($data);
        return $books;
    }


    /**
     * Gets all books from the data source
     * @param int $id bookID
     * @return array An array containing book objects
     */
    public static function getAll(): array
    {
       
        $books = self::loadData();
        $bookobjects = [];
        //Mapping der Daten aus dem JSON zu Book-Objekten
        foreach ($books as $key => $value) {
            $book = new Book($value->id, $value->title, $value->price, $value->stock);
            array_push($bookobjects, $book);
        }
        return $bookobjects;
    }


    /**
     * Gets a book by its id from the data source
     * @param int $id bookID
     * @return self|bool The searched book or false on failure
     */
    public static function get(int $id): self | bool
    {
        $books = self::loadData();
        foreach ($books as $key => $value) {
            if ($value->id == $id)
            {
                $book = new Book($value->id, $value->title, $value->price, $value->stock);
                return $book;
            }
        }
        return false;
    }


    /**
     * Gets a book by its id from the data source
     * Now with Exceptions
     * @param int $id bookID
     * @return self|bool The searched book or false on failure
     * @throws Exception Book not found
     */
    public static function get2(int $id): self | bool
    {
        $books = self::loadData();
        foreach ($books as $key => $value) {
            if ($value->id == $id)
            {
                //Todo: add Exception to constructor, if object creation fails and catch/rethrow exception here
                $book = new Book($value->id, $value->title, $value->price, $value->stock);
                return $book;
            }
        }
        throw new Exception('Book: book not found');
    }
    

    /**
     * Get the value of id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of stock
     *
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * Set the value of stock
     *
     * @param int $stock
     *
     * @return self
     */
    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }
}