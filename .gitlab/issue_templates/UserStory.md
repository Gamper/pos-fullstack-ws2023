# User Story
Ich als ROLLE möchte FEATURE um ZIEL/BUSINESS VALUE zu erreichen.

+ Weitere Beschreibung

## Tasks

- [ ] Task 1
- [ ] Task 2
- [ ] Task 3

## Acceptance criteria

- [ ] AC 1
- [ ] AC 2

## Definition of done
- [ ] Tests in place
- [ ] Documented source code
- [ ] User manual updated

